SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- 
-- Database: `mchn_chouette`

-- Table structure for table `entrevues`
--
CREATE TABLE IF NOT EXISTS `entrevues` (
  `ntrvs_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `title` varchar(100),
  `content` LONGTEXT NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ntrvs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `paragraphs`
--
CREATE TABLE IF NOT EXISTS `paragraphs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ntrvs_id` bigint(20) NOT NULL,
  `prgrph_id` bigint(20) NOT NULL,
  `paragraph` LONGTEXT NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `user`
--
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL UNIQUE,
  `password` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `unite_h`
--
CREATE TABLE IF NOT EXISTS `unite_h` (
  `unite_id` bigint(20)  NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `old_title` varchar(1000) ,
  `user_id` bigint(20) NOT NULL UNIQUE,
  `updated_by` bigint(20),
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`unite_id`),
   CONSTRAINT `user_id_unite` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Table structure for table `unite_h_comment`
--
CREATE TABLE IF NOT EXISTS `unite_h_comment` (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(250) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `unite_id` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`),
  CONSTRAINT `unite_id_comment` FOREIGN KEY (`unite_id`) REFERENCES `unite_h` (`unite_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_id_comment` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


--
-- Table structure for table `unite_h_history`
--
CREATE TABLE IF NOT EXISTS `unite_h_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unite_id` bigint(20) NOT NULL UNIQUE,
  `old_value` varchar(250) NOT NULL,
  `new_value` varchar(250) NOT NULL,
  `user_id` bigint(20) NOT NULL UNIQUE,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_id`),
  CONSTRAINT `unite_id_history` FOREIGN KEY (`unite_id`) REFERENCES `unite_h` (`unite_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_id_history` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `annotation`
--
CREATE TABLE IF NOT EXISTS `annotation` (
  `annotation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL UNIQUE,
  `unite_id` bigint(20) NOT NULL UNIQUE,
  `ntrvs_id` bigint(20) NOT NULL UNIQUE,
  `paragraph_id` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`annotation_id`),
  CONSTRAINT `unite_id_annotation` FOREIGN KEY (`unite_id`) REFERENCES `unite_h` (`unite_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_id_annotation` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ntrvs_id_annotation` FOREIGN KEY (`ntrvs_id`) REFERENCES `entrevues` (`ntrvs_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocUserSet`(
INOUT p_user_id bigint(20),
IN p_first_name  VARCHAR(100), 
IN p_last_name VARCHAR(100),
IN p_password  VARCHAR(255),  
IN p_email      VARCHAR(100)
)
BEGIN
DECLARE MsgParm1               VARCHAR(200);
DECLARE operation_type         VARCHAR(10);
DECLARE user_id_exists  	 INT;
DECLARE p_first_name_update  VARCHAR(200); 
DECLARE p_last_name_update  VARCHAR(100);    
DECLARE Parmy CONDITION FOR SQLSTATE '99001';
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

-- DECLARE exit handler for sqlwarning
-- BEGIN
    -- WARNING
-- ROLLBACK;
-- END;
START TRANSACTION;


-- insert

	SET MsgParm1 = CASE
					   WHEN p_first_name          IS NULL THEN 'The 3rd parameter (p_first_name) requires a value'
					   WHEN p_last_name      IS NULL THEN 'The 4th parameter (p_last_name) requires a value'
					   WHEN p_password      IS NULL THEN 'The 5th parameter (p_password) requires a value'
					   WHEN p_email      IS NULL THEN 'The 6th parameter (p_email) requires a value'
					   ELSE                   'Parameters assigned'
	END;
	IF MsgParm1 <> 'Parameters assigned'
	THEN
		SIGNAL Parmy
		  SET MESSAGE_TEXT = MsgParm1;
	END IF;

	INSERT IGNORE INTO user (first_name, last_name,password,email)
	VALUES (p_first_name, p_last_name,p_password, p_email);
	/*Return latest attributeId to the caller so it can be used */
	SET p_user_id = last_insert_id();
	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocUniteHDelete`(IN `p_unite_id` BIGINT)
    NO SQL
BEGIN
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
  END;
START TRANSACTION;
	DELETE FROM `unite_h_comment` WHERE `unite_id` = p_unite_id;
	DELETE FROM `unite_h` WHERE `unite_id`= p_unite_id;
   	COMMIT; 
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocUniteHSet`(
INOUT p_unite_id BIGINT(20),
IN p_title VARCHAR(255), 
IN p_user_id DOUBLE
)
BEGIN
DECLARE MsgParm1               VARCHAR(200);
DECLARE operation_type         VARCHAR(10);
DECLARE new_unite_id BIGINT;
DECLARE Parmy CONDITION FOR SQLSTATE '99001';
DECLARE p_title_up VARCHAR(255); 
DECLARE p_old_title_up VARCHAR(255); 
DECLARE p_updated_by_up INT; 

DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

-- DECLARE exit handler for sqlwarning
-- BEGIN
    -- WARNING
-- ROLLBACK;
-- END;
START TRANSACTION;
SET operation_type = 'UPDATE';
IF  p_unite_id = 0 OR p_unite_id IS NULL
THEN
	SET operation_type = 'INSERT';
END IF;
-- insert
IF  operation_type = 'INSERT'
THEN
    INSERT IGNORE INTO unite_h ( title, old_title, user_id, updated_by)
    VALUES (p_title, null, p_user_id,null);
	set p_unite_id = last_insert_id();
	COMMIT;
END IF;

IF  operation_type = 'UPDATE'
THEN
    /***First , select existing values, and use them if any parameters are sent empty*/
	SELECT `title`, `old_title`
    INTO `p_title_up`, `p_old_title_up`
    FROM `unite_h` WHERE `unite_id` = `p_unite_id`;

    IF p_title IS NOT NULL AND p_title != '' 
    	THEN set p_old_title_up = p_title_up; 
         set p_title_up = p_title;
         set p_updated_by_up = p_user_id;
    END IF;
	
	UPDATE unite_h 
	SET title= p_title_up,
	old_title= p_old_title_up,
	updated_by= p_updated_by_up
	WHERE unite_id =p_unite_id;
    COMMIT;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocParagraphSet`(
OUT p_value_id bigint(20),
IN p_ntrvs_id bigint(20),
IN p_prgrph_id  bigint(20),
IN p_paragraph LONGTEXT
)
BEGIN
DECLARE MsgParm1               VARCHAR(200); 
DECLARE Parmy CONDITION FOR SQLSTATE '99001';
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

-- DECLARE exit handler for sqlwarning
-- BEGIN
    -- WARNING
-- ROLLBACK;
-- END;
START TRANSACTION;


-- insert

	SET MsgParm1 = CASE
					   WHEN p_ntrvs_id          IS NULL THEN 'The 3rd parameter (p_ntrvs_id) requires a value'
					   WHEN p_prgrph_id      IS NULL THEN 'The 4th parameter (p_prgrph_id) requires a value'
					   WHEN p_paragraph      IS NULL THEN 'The 5th parameter (p_paragraph) requires a value'
	END;
	IF MsgParm1 <> 'Parameters assigned'
	THEN
		SIGNAL Parmy
		  SET MESSAGE_TEXT = MsgParm1;
	END IF;

	INSERT IGNORE INTO paragraphs (ntrvs_id, prgrph_id,paragraph)
	VALUES (p_ntrvs_id, p_prgrph_id,p_paragraph);
	/*Return latest attributeId to the caller so it can be used */
	SET p_value_id = last_insert_id();
	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocEntrevueSet`(
INOUT p_ntrvs_id bigint(20),
IN p_user_id bigint(20),
IN p_title VARCHAR(200),
IN p_content  LONGTEXT
)
BEGIN
DECLARE MsgParm1               VARCHAR(200); 
DECLARE Parmy CONDITION FOR SQLSTATE '99001';
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

-- DECLARE exit handler for sqlwarning
-- BEGIN
    -- WARNING
-- ROLLBACK;
-- END;
START TRANSACTION;


-- insert

	SET MsgParm1 = CASE
					   WHEN p_content      IS NULL THEN 'The 5th parameter (p_content) requires a value'
	END;
	IF MsgParm1 <> 'Parameters assigned'
	THEN
		SIGNAL Parmy
		  SET MESSAGE_TEXT = MsgParm1;
	END IF;

	INSERT IGNORE INTO entrevues (title, content,user_id)
	VALUES (p_title, p_content,p_user_id);
	/*Return latest attributeId to the caller so it can be used */
	SET p_ntrvs_id = last_insert_id();
	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocUniteHCommentSet`(
OUT p_comment_id bigint(20),
IN p_comment VARCHAR(100),
IN p_user_id  bigint(20),
IN p_unite_id  bigint(20)
)
BEGIN
DECLARE MsgParm1               VARCHAR(200); 
DECLARE Parmy CONDITION FOR SQLSTATE '99001';
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

-- DECLARE exit handler for sqlwarning
-- BEGIN
    -- WARNING
-- ROLLBACK;
-- END;
START TRANSACTION;


-- insert

	SET MsgParm1 = CASE
					   WHEN p_comment      IS NULL THEN 'The parameter (p_comment) requires a value'
	END;
	IF MsgParm1 <> 'Parameters assigned'
	THEN
		SIGNAL Parmy
		  SET MESSAGE_TEXT = MsgParm1;
	END IF;

	INSERT IGNORE INTO unite_h_comment (comment, user_id,unite_id)
	VALUES (p_comment, p_user_id, p_unite_id);
	/*Return latest attributeId to the caller so it can be used */
	SET p_comment_id = last_insert_id();
	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sprocEntrevueDelete`(IN `p_ntrv_id` BIGINT)
    NO SQL
BEGIN
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
  END;
START TRANSACTION;
	DELETE FROM `annotation` WHERE `ntrvs_id` = p_ntrv_id;
	DELETE FROM `paragraphs` WHERE `ntrvs_id` = p_ntrv_id;
	DELETE FROM `entrevues` WHERE `ntrvs_id`= p_ntrv_id;
   	COMMIT; 
END$$
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;