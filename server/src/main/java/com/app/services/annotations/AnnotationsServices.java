package com.app.services.annotations;

import com.app.model.annotations.Annotations;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.users.User;

/**
 * 
 * @author hfoko
 *
 */
public interface AnnotationsServices {
	/**
	 * 
	 * @param unite
	 * @param user
	 * @return
	 * @throws Exception
	 */
	Annotations createAnnotation(UniteH unite, User user)throws Exception;
	
	/**
	 * 
	 * @param annotation_id
	 * @return
	 * @throws Exception
	 */
	Boolean deletAnnotation(Long annotation_id)throws Exception;

}
