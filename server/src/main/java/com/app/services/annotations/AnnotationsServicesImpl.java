package com.app.services.annotations;

import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.app.dao.annotations.AnnotationsDAO;
import com.app.exceptions.ServiceException;
import com.app.model.annotations.Annotations;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.users.User;
import com.app.utils.Constants;
import com.app.utils.Constants.ServiceExceptionType;

@Service("adServices")
public class AnnotationsServicesImpl implements AnnotationsServices {
	
	private static final Logger LOG = Logger.getLogger(AnnotationsServicesImpl.class);

	//@Autowired
	//private AnnotationsDAO annotationsDao;

	@Override
	public Annotations createAnnotation(UniteH unite, User user) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public Boolean deletAnnotation(Long annotation_id) throws Exception {
		try {
			return null;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}


}
