package com.app.services.entrevue;

import java.util.List;
import com.app.model.entrevue.Entrevue;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public interface EntrevueService {
	/**
	 * 
	 * @param 
	 * @return
	 * @throws Exception
	 */
	List<Entrevue> getEntrevues() throws Exception;
	
	/**
	 * 
	 * @param entrevue_id
	 * @return
	 * @throws Exception
	 */
	Entrevue getEntrevueById(Long entrevue_id) throws Exception;
	
	/**
	 * 
	 * @param entrevue
	 * @return
	 * @throws Exception
	 */
	Entrevue createEntrevue(String entrevue,String title, User user) throws Exception;

	/**
	 * 
	 * @param entrevue_id
	 * @return
	 * @throws Exception
	 */
	Boolean deleteEntrevue(Long entrevue_id) throws Exception;

}
