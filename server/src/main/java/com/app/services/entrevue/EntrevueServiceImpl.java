package com.app.services.entrevue;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.dao.entrevue.EntrevueDao;
import com.app.exceptions.ServiceException;
import com.app.model.entrevue.Entrevue;
import com.app.model.users.User;
import com.app.services.paragraph.ParagraphService;
import com.app.utils.Constants;
import com.app.utils.Constants.ServiceExceptionType;

@Service
public class EntrevueServiceImpl implements EntrevueService {
	private static final Logger LOG = Logger.getLogger(EntrevueServiceImpl.class);

	@Autowired
	private EntrevueDao entrevueDao;
	@Autowired
	private ParagraphService paragraphService;

	@Override
	public List<Entrevue> getEntrevues() throws Exception {
		try {
			List<Entrevue> entrevues = entrevueDao.getAllEntrevue();
			for (Entrevue entrevue : entrevues){
				entrevue.setParagraphes(paragraphService.getParagraphs(entrevue.getId()));
			}
			return entrevues;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public Entrevue getEntrevueById(Long entrevue_id) throws Exception {
		try {
			Entrevue entrevue = entrevueDao.getEntrevueById(entrevue_id);
			entrevue.setParagraphes(paragraphService.getParagraphs(entrevue_id));
			return entrevue;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public Entrevue createEntrevue(String entrevue, String title, User user) throws Exception {
		try {
			Entrevue entrevueObj = entrevueDao.saveEntrevue(new Entrevue(entrevue,title,user));
			paragraphService.setParagraphs(entrevueObj);
			return entrevueObj;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public Boolean deleteEntrevue(Long entrevue_id) throws Exception {
		try {
			return entrevueDao.deleteEntrevue(entrevue_id);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	

}
