/**
 * 
 */
package com.app.services.users;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
//import com.app.configuration.AppConfiguration;
import com.app.dao.users.UserDao;
import com.app.exceptions.ServiceException;
import com.app.model.users.User;
import com.app.utils.Constants;
import com.app.utils.Constants.ServiceExceptionType;

/**
 * @author hfoko
 *
 */
@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
	private static final int MAX_LENGTH = 8;
	@Autowired
	private UserDao userDao;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	//@Autowired
	//private AppConfiguration appConfiguration;
	public UserServiceImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.services.users.UserService#getUserById(java.lang.Long)
	 */
	@Override
	public User getUserById(Long id) throws Exception {
		try {
			return userDao.getUserById(id);
		} catch (Exception e) {
			LOG.error("An error occured while getting user by id [" + id + "]", e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_USER, Constants.UNKNOWN_ERROR);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.services.users.UserService#getUserByName(java.lang.String)
	 */
	@Override
	public User getUserByName(String username) throws Exception {
		try {
			return userDao.getUserByName(username);
		} catch (Exception e) {
			LOG.error("An error occured while getting user by username [" + username + "]", e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_USER, Constants.UNKNOWN_ERROR);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.services.users.UserService#getUsers()
	 */
	@Override
	public List<User> getUsers() throws Exception {
		try {
			return userDao.getUsers();
		} catch (Exception e) {
			LOG.error("An error occured while getting users", e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_USER, Constants.UNKNOWN_ERROR);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.services.users.UserService#add(com.app.model.users.User)
	 */
	@Override
	public void createUser(User user) throws Exception {
		User userDb = null;
		try {
			userDb = userDao.getUserByName(user.getEmail());
		} catch (EmptyResultDataAccessException e) {
		}
		if (userDb == null) {
			try {
				String encryptedPassword = passwordEncoder.encode(user.getPassword());
				user.setPassword(encryptedPassword);
				userDao.createUser(user);
				
				
			} catch (Exception e) {
				throw new ServiceException(ServiceExceptionType.INFO, Constants.ERROR_CODE_USER, Constants.USER_INVALID_EMAIL);
			}
		} else {
			throw new ServiceException(ServiceExceptionType.INFO, Constants.ERROR_CODE_USER, Constants.USER_INVALID_EMAIL);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.services.users.UserService#update(com.app.model.users.User)
	 */
	@Override
	public User updateUser(User user) throws Exception {
		User userDb = null;
		try {
			userDb = userDao.getUserById(user.getId());
			String firstName = userDb.getFirstName();
			String lastName = userDb.getLastName();
			if (user.getFirstName() != null && !user.getFirstName().equals(firstName)) {
				userDb.setFirstName(user.getFirstName());
			}
			if (user.getLastName() != null && !user.getLastName().equals(lastName)) {
				userDb.setLastName(user.getLastName());
			}
			return userDao.updateUser(userDb);
		} catch (EmptyResultDataAccessException e) {
			LOG.error("An error occured while updating user", e);
			throw new ServiceException(ServiceExceptionType.WARN, Constants.ERROR_CODE_USER, Constants.USER_ERROR_UPDATE);
		}
	}

	
	@Override
	public void updatePassword(Long userId, String password, String newPassword) throws Exception {
		User user = userDao.getUserById(userId);
		if (user != null) {
			if(passwordEncoder.matches(password, user.getPassword())){
				if (!passwordEncoder.matches(newPassword, user.getPassword())) {
					userDao.updatePassword(userId, passwordEncoder.encode(newPassword));
				} else {
					throw new ServiceException(ServiceExceptionType.ERROR, Constants.ERROR_CODE_USER, Constants.PASSWORD_NOT_MATCHING);
				}
			}else{
				throw new ServiceException(ServiceExceptionType.ERROR, Constants.ERROR_CODE_USER, Constants.CURRENT_PASSWORD_NOT_MATCHING);
			}
		}
	}

	@Override
	public void resetPassword(String email) throws Exception {
		User user = userDao.getUserByName(email);
		final String tmpPassword = RandomStringUtils.randomAlphabetic(MAX_LENGTH);
		if (user != null) {
			try {
				userDao.updatePassword(user.getId(), passwordEncoder.encode(tmpPassword));
				// send notifications message to users.
				try {
					final Set<String> emails = new HashSet<>();
					emails.add(email);
					
				} catch (Exception e) {
					LOG.warn(Constants.NOTIFICATION_ERROR, e);
				}
			} catch (Exception e) {
				LOG.error("An error occured while resetting password", e);
				throw new ServiceException(ServiceExceptionType.ERROR, Constants.ERROR_CODE_USER, Constants.USER_ERROR_UPDATE_PASSWORD);
			}
		}
	}

	@Override
	public List<Long> getUsersId() throws Exception {
		try {
			return userDao.getUsersId();
		} catch (Exception e) {
			LOG.error("An error occured while getting users ids", e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_USER, Constants.UNKNOWN_ERROR);
		}
	}


}
