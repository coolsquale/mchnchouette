/**
 * 
 */
package com.app.services.users;

import java.util.List;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public interface UserService {
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	User getUserById(Long id) throws Exception;
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	User getUserByName(String username) throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<User> getUsers() throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Long> getUsersId() throws Exception;
	
	
	/**
	 * 
	 * @param user
	 * @throws Exception
	 */
	void createUser(User user) throws Exception;
	
	/**
	 * 
	 * @param user
	 * @throws Exception
	 */
	User updateUser(User user) throws Exception;


	/**
	 * 
	 * @param newPassword
	 * @throws Exception
	 */
	void updatePassword(Long userId, String password, String newPassword) throws Exception;

	/**
	 * 
	 * @param email
	 * @throws Exception
	 */
	void resetPassword(String email) throws Exception;

}
