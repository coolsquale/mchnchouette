/**
 * 
 */
package com.app.services.paragraph;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.dao.paragraph.ParagraphDao;
import com.app.exceptions.ServiceException;
import com.app.model.entrevue.Entrevue;
import com.app.model.paragraph.Paragraph;
import com.app.model.unitehemeneutique.UniteH;
import com.app.utils.Constants;
import com.app.utils.Constants.ServiceExceptionType;

/**
 * @author hfoko
 *
 */
@Service
public class ParagraphServiceImpl implements ParagraphService {

	private static final Logger LOG = Logger.getLogger(ParagraphServiceImpl.class);
	@Autowired
	private ParagraphDao paragraphDao;

	@Override
	public List<Paragraph> getParagraphs(Long entrevue_id) throws Exception {
		try {
			return paragraphDao.getParagraphs(entrevue_id);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public List<UniteH> getUniteHbyParagraph(Long paragraph_id) throws Exception {
		try {
			//should call anotationDao to get unite from paragraph
			return null;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}
	@Override
	public void addUniteH(Long paragraph_id, UniteH unite) throws Exception {
		try {
			//should call anotationDao to add an unite to paragraph
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
		
	}
	@Override
	public Boolean deleteUniteH(Long paragraph_id, Long unite_id) throws Exception {
		try {
			//should call anotationDao to delete an unite from paragraph
			return true;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public void setParagraphs(Entrevue entrevue) throws Exception {
		try {
			paragraphDao.setParagraphs(entrevue);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}
	
	
}
