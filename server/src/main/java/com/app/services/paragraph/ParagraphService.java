/**
 * 
 */
package com.app.services.paragraph;

import java.util.List;

import com.app.model.entrevue.Entrevue;
import com.app.model.paragraph.Paragraph;
import com.app.model.unitehemeneutique.UniteH;

/**
 * @author hfoko
 *
 */
public interface ParagraphService {

	/**
	 * 
	 * @param entrevue_id
	 * @return
	 * @throws Exception
	 */
	List<Paragraph> getParagraphs(Long entrevue_id) throws Exception;
	
	/**
	 * 
	 * @param entrevue_id
	 * @param paragraphs
	 * @return
	 * @throws Exception
	 */
	void setParagraphs(Entrevue entrevue) throws Exception;
	
	
	/**
	 * 
	 * @param entrevue_id
	 * @return
	 * @throws Exception
	 */
	List<UniteH> getUniteHbyParagraph(Long paragraph_id) throws Exception;

	/**
	 * 
	 * @param paragraph_id
	 * @return
	 * @throws Exception
	 */
	void addUniteH(Long paragraph_id, UniteH unite) throws Exception;
	
	/**
	 * 
	 * @param paragraph_id
	 * @param unite_id
	 * @return
	 * @throws Exception
	 */
	Boolean deleteUniteH(Long paragraph_id, Long unite_id) throws Exception;
	
}
