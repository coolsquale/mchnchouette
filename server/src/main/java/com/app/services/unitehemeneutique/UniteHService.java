/**
 * 
 */
package com.app.services.unitehemeneutique;

import java.util.List;

import com.app.model.unitehemeneutique.UniteH;
import com.app.model.unitehemeneutique.UniteHComment;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public interface UniteHService {

	/**
	 * 
	 * @param id 
	 * @param title
	 * @param user
	 * @param createdOn
	 * @throws Exception
	 */
	UniteH createUniteH(String title, User user) throws Exception;
	
	/**
	 * 
	 * @param id
	 * @throws Exception
	 */
	Boolean deleteUniteH(UniteH unite) throws Exception;

	/**
	 * 
	 * @param id
	 * @param title
	 * @param user
	 * @throws Exception
	 */
	Boolean updateUniteH(UniteH unite) throws Exception;

	/**
	 * 
	 * @param unite_id
	 * @return
	 * @throws Exception
	 */
	UniteH getUniteHById(Long unite_id) throws Exception;

	/**
	 * 
	 * @param paragraphId
	 * @return
	 * @throws Exception
	 */
	List<UniteH> getUniteHByParagraphId(Long paragraphId) throws Exception;
	
	/**
	 * 
	 * @param paragraphId
	 * @return
	 * @throws Exception
	 */
	List<UniteH> getAllUniteH() throws Exception;
	
	
	/**
	 * 
	 * @param unite_id
	 * @return
	 * @throws Exception
	 */
	List<UniteHComment> getUniteHCommentByUniteId(Long unite_id) throws Exception;
	
	/**
	 * 
	 * @param unite_id
	 * @return
	 * @throws Exception
	 */
	void addUniteHComment(Long unite_id,UniteHComment comment) throws Exception;
	/**
	 * 
	 * @param unite_id
	 * @return
	 * @throws Exception
	 */
	//void deleteUniteHComment(Long unite_id,UniteHComment comment) throws Exception;




}
