/**
 * 
 */
package com.app.services.unitehemeneutique;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.dao.unitehemeneutique.UniteHDao;
import com.app.exceptions.ServiceException;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.unitehemeneutique.UniteHComment;
import com.app.model.users.User;
import com.app.utils.Constants;
import com.app.utils.Constants.ServiceExceptionType;

/**
 * @author hfoko
 *
 */
@Service
public class UniteHServiceImpl implements UniteHService {

	private static final Logger LOG = Logger.getLogger(UniteHServiceImpl.class);
	@Autowired
	private UniteHDao uniteHDao;

	@Override
	public UniteH createUniteH(String title, User user) throws Exception {
		try {
			return uniteHDao.createUniteH(title, user);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
		
	}

	@Override
	public Boolean deleteUniteH(UniteH unite) throws Exception {
		try {
			return uniteHDao.deleteUniteH(unite);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
		
	}

	@Override
	public Boolean updateUniteH(UniteH unite) throws Exception {
		try {
			return uniteHDao.updateUniteH(unite);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
		
	}

	@Override
	public UniteH getUniteHById(Long unite_id) throws Exception {
		try {
			UniteH unite = uniteHDao.getUniteHById(unite_id);
			final List <UniteHComment> comments = getUniteHCommentByUniteId(unite_id);
			unite.setComments(comments);
			LOG.error(unite.getComments().size());
			return unite;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public List<UniteH> getUniteHByParagraphId(Long paragraphId) throws Exception {
		try {
			return uniteHDao.getAllUniteH(paragraphId);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public List<UniteHComment> getUniteHCommentByUniteId(Long unite_id) throws Exception {
		try {
			return uniteHDao.getAllUniteHComment(unite_id);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

	@Override
	public void addUniteHComment(Long unite_id, UniteHComment comment) throws Exception {
		try {
			uniteHDao.AddUniteHComment(unite_id, comment);
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
		
	}

	@Override
	public List<UniteH> getAllUniteH() throws Exception {
		try {
			List<UniteH> unites = uniteHDao.getAllUniteH();
			for(UniteH unite : unites){
				unite.setComments(getUniteHCommentByUniteId(unite.getId()));
			}
			return unites;
		} catch (Exception e) {
			LOG.error("An error occured ",e);
			throw new ServiceException(ServiceExceptionType.UNKNOWN, Constants.ERROR_CODE_TRANSACTION, Constants.TRANSACTION_CREATION_ERROR);
		}
	}

}
