/**
 * 
 */
package com.app.api.model;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author hfoko
 *
 */
public class IParagraph implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long getParagraphId;
	private String paragraph;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	
	public IParagraph() {
	}

	public Long getParagraphId() {
		return getParagraphId;
	}

	public void setParagraphId(Long getParagraphId) {
		this.getParagraphId = getParagraphId;
	}

	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = paragraph;
	}
	
	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

}
