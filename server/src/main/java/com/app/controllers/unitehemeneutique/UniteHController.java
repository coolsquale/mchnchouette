package com.app.controllers.unitehemeneutique;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.unitehemeneutique.UniteHComment;
import com.app.model.unitehemeneutique.UniteHCommentTO;
import com.app.model.unitehemeneutique.UniteHTO;
import com.app.model.users.User;
import com.app.services.unitehemeneutique.UniteHService;
import com.app.services.users.UserService;
import com.app.utils.Constants;
import com.app.utils.Constants.HttpStatus;
import com.app.utils.Data;

@RestController
@RequestMapping(value =  Constants.URL_UNITEH)
public class UniteHController {
	
	@Autowired
	private UniteHService uniteHServices;
	@Autowired
	private UserService userService;
	private static final Logger LOG = Logger.getLogger(UniteHController.class);
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Data createUniteH(@RequestBody UniteHTO uniteHTO) throws Exception {
		try {
	    		final ModelMapper mapper = new ModelMapper();
				final User user = userService.getUserById(uniteHTO.getUserId());
				final UniteH unite = uniteHServices.createUniteH(uniteHTO.getTitle(), user);
				final UniteHTO result = mapper.map( unite, UniteHTO.class);
				return Data.ok(result);
		} catch (Exception e) {
			return new Data<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public Data updateUniteH(@RequestBody UniteHTO uniteHTO) throws Exception {
		try {
	    		final ModelMapper mapper = new ModelMapper();
				final User user = userService.getUserById(uniteHTO.getUserId());
				final UniteH unite_up = mapper.map(uniteHTO, UniteH.class);
				unite_up.setUser(user);
				final Boolean result = uniteHServices.updateUniteH(unite_up);
				return Data.ok(result);
		} catch (Exception e) {
			return new Data<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public Data getAllUnitesH() throws Exception {
		final ModelMapper mapper = new ModelMapper();
		try {
			List<UniteH> unites = uniteHServices.getAllUniteH();
			if (unites != null) {
				List<UniteHTO> unitesTO = unites.stream().map(unite -> mapper.map(unite, UniteHTO.class))
						.collect(Collectors.toList());
				return Data.found(unitesTO);
			} else {
				return Data.notFound(Constants.REQUEST_EMPTY_RESULT);
			}
		} catch (Exception e) {
			return new Data<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{uniteId}", method = RequestMethod.GET)
	public Data getUniteHById(@PathVariable("uniteId")Long uniteId) throws Exception {
		final ModelMapper mapper = new ModelMapper();
		try {
			UniteH unite = uniteHServices.getUniteHById(uniteId);
			if (unite != null) {
				UniteHTO uniteTO = mapper.map(unite, UniteHTO.class);
				uniteTO.setComments(unite.getComments().stream().map(com -> mapper.map(com, UniteHCommentTO.class))
					.collect(Collectors.toList()));
				return Data.found(uniteTO);
			} else {
				return Data.notFound(Constants.REQUEST_EMPTY_RESULT);
			}
		} catch (Exception e) {
			return new Data<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
    @SuppressWarnings("rawtypes")
   	@RequestMapping(value = "/delete/{uniteId}", method = RequestMethod.DELETE)
       public Data deleteId(@PathVariable("uniteId") Long uniteId) {
       	try {
       		final UniteH unite = uniteHServices.getUniteHById(uniteId);
       		final Boolean deleted = uniteHServices.deleteUniteH(unite);
   			return Data.ok(deleted);
   		} catch (Exception e) {
   			LOG.error("An error occured while getting annotations.",e);
   			return Data.internalServerError(e);
   		}
          
   }
    
    @SuppressWarnings("rawtypes")
   	@RequestMapping(value = "/comment/{uniteId}", method = RequestMethod.POST)
       public Data commentUniteH(@PathVariable("uniteId") Long uniteId, @RequestBody UniteHCommentTO commentTO) {
    	final ModelMapper mapper = new ModelMapper();
       	try {
       		final User user = userService.getUserById(commentTO.getUser().getId());
       		final UniteHComment comment = mapper.map(commentTO, UniteHComment.class);
       		comment.setUser(user);
       		uniteHServices.addUniteHComment(uniteId, comment);
   			return Data.ok();
   		} catch (Exception e) {
   			LOG.error("An error occured while getting annotations.",e);
   			return Data.internalServerError(e);
   		}
          
   }
    
    @SuppressWarnings("rawtypes")
   	@RequestMapping(value = "/comments/{uniteId}", method = RequestMethod.GET)
       public Data commentUniteH(@PathVariable("uniteId") Long uniteId) {
    	final ModelMapper mapper = new ModelMapper();
       	try {
       		final List <UniteHComment> comments = uniteHServices.getUniteHCommentByUniteId(uniteId);
       		List<UniteHCommentTO> commentsTO = comments.stream().map(com -> mapper.map(com, UniteHCommentTO.class))
					.collect(Collectors.toList());
   			return Data.ok(commentsTO);
   		} catch (Exception e) {
   			LOG.error("An error occured while getting annotations.",e);
   			return Data.internalServerError(e);
   		}
          
   }
}
