/**
 * 
 */
package com.app.controllers.users;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.app.model.users.ChangePassword;
import com.app.model.users.User;
import com.app.model.users.UserTO;
import com.app.services.users.UserService;
import com.app.utils.Constants;
import com.app.utils.Data;

/**
 * @author hfoko
 *
 */
@RestController
@RequestMapping(value = Constants.URL_USERS)
public class UserController {
	
	private static final Logger LOG = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public Data getUsers() throws Exception {
		try {
			List<User> users = userService.getUsers();
			final ModelMapper mapper = new ModelMapper();
			return Data.found(users.stream().map(user -> mapper.map(user, UserTO.class)).collect(Collectors.toList()));
		} catch (Exception e) {
			LOG.error("Error while getting users.",e);
			return Data.internalServerError(e);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Data getUser(@PathVariable("id") Long id) throws Exception {
		try {
			final ModelMapper mapper = new ModelMapper();
			return Data.found(mapper.map(userService.getUserById(id), UserTO.class));
		} catch (Exception e) {
			LOG.error("Error while getting users with id [" + id + "]", e);
			return Data.internalServerError(e);
		}
	}
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/names/", method = RequestMethod.GET)
	public Data getUserByName(@RequestParam(value = "username") String username) throws Exception {
		try {
			User user = userService.getUserByName(username);
			final ModelMapper mapper = new ModelMapper();
			return Data.found(mapper.map(user, UserTO.class));
		} catch (Exception e) {
			LOG.error("Error while getting users with username [" + username + "]", e);
			return Data.internalServerError(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Data createUser(@RequestBody UserTO userTO) throws Exception {
		try {
			LOG.info("Creating user.");
			final ModelMapper mapper = new ModelMapper();
			userService.createUser(mapper.map(userTO, User.class));
			return Data.ok();
		} catch (Exception e) {
			LOG.error("Error while creating user.",e);
			return Data.internalServerError(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
	public Data updateUser(@RequestBody UserTO userTO, @PathVariable("userId") Long userId) throws Exception {
		try {
			final ModelMapper mapper = new ModelMapper();
			User user = mapper.map(userTO, User.class);
			User updateduser = userService.updateUser(user);
			return Data.ok(mapper.map(updateduser, UserTO.class));
		} catch (Exception e) {
			LOG.error("Error while updating user with id [" + userId + "]", e);
			return Data.internalServerError(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/password/{userId}", method = RequestMethod.PUT)
	public Data updatePassword(@RequestBody ChangePassword changePassword, @PathVariable("userId") Long userId)
			throws Exception {
		try {
			userService.updatePassword(userId, changePassword.getPassword(),changePassword.getNewPassword());
			return Data.ok();
		} catch (Exception e) {
			LOG.error("Error while updating password for user wth id [" + userId + "]", e);
			return Data.internalServerError(e);
		}
	}
}
