/**
 * 
 */
package com.app.controllers.annotations;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.app.utils.Constants;
import com.app.model.annotations.AnnotationsTO;
import com.app.services.annotations.AnnotationsServices;
import com.app.utils.Data;

/**
 * @author hfoko
 *
 */
@RestController
@RequestMapping(value = Constants.URL_ANNOTATION)
public class AnnotationsController {
	
	private static final Logger LOG = Logger.getLogger(AnnotationsController.class);
	
	@Autowired
	private AnnotationsServices annotationsServices;
	
	/**
	 * get default ads, recent ads
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/default/", method = RequestMethod.GET)
	public Data getAnnotations(@PathVariable("uniteId") Long uniteId,@RequestBody AnnotationsTO annotationsTO) throws Exception {
		try {
			return Data.notFound();
		} catch (Exception e) {
			LOG.error("An error occured while getting annotations.",e);
			return Data.internalServerError(e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Data createAnnotations(@RequestBody AnnotationsTO annotationTO) throws Exception {
		try {
			//final ModelMapper mapper = new ModelMapper();
			//final TripAd adToCreate = mapper.map(adTO, TripAd.class);
			//final TripAd tripAd = adServices.saveTripAd(adToCreate);
			//final TripAdTO ad = mapper.map( tripAd, TripAdTO.class);
			return Data.ok();
		} catch (Exception e) {
			LOG.error("An error occured while creating an annotation.",e);
			return Data.internalServerError(e);
		}
	}
	
}
