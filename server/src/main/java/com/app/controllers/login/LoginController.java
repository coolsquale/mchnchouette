/**
 * 
 */
package com.app.controllers.login;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.app.model.users.NewUser;
import com.app.model.users.User;
import com.app.services.users.UserService;
import com.app.utils.Constants;
import com.app.utils.Data;

/**
 * @author hfoko
 *
 */
@RestController
public class LoginController {
	
	private static final Logger LOG = Logger.getLogger(LoginController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private DefaultTokenServices tokenServices;
	
	@RequestMapping(value = Constants.URL_LOGOUT, method = RequestMethod.POST)
	public void logout(HttpServletRequest request) {
		invalidateSession(request);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = Constants.DEFAULT_USER, method = RequestMethod.POST)
	public Data defaultuser() throws Exception {
		try {
			LOG.info("Creating Default user.");
			final User user = new User("app", "admin","0123456789", "admin@gmail.com");
			userService.createUser(user);
			return Data.ok(Boolean.TRUE);
		} catch (Exception e) {
			LOG.error("Error while creating default user.",e);
			return Data.internalServerError(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = Constants.SIGN_UP, method = RequestMethod.POST)
	public Data signup(@RequestBody NewUser newUser) throws Exception {
		try {
			LOG.info("Creating user.");
			final User user = new User(newUser.getFirstname(), newUser.getLastname(),newUser.getPassword(), newUser.getEmail());
			userService.createUser(user);
			return Data.ok(Boolean.TRUE);
		} catch (Exception e) {
			LOG.error("Error while creating user.",e);
			return Data.internalServerError(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = Constants.RESET_PASSWORD, method = RequestMethod.POST)
	public Data resetPassword(@RequestParam(value = "email") String email) throws Exception {
		try {
			userService.resetPassword(email);
			return Data.ok(Boolean.TRUE);
		} catch (Exception e) {
			return Data.internalServerError(e);
		}
	}

	/**
	 * 
	 * @param request
	 */
	private void invalidateSession(HttpServletRequest request) {
		if (request.getSession() != null) {
			request.getSession().invalidate();
		}
		//revokeToken(request);
		
	}
	/*private void revokeToken(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");
		String tokenValue = authHeader.replace("bearer", "").trim();
		tokenServices.revokeToken(tokenValue);
		//tokenServices.setAccessTokenValiditySeconds(1);
		//tokenServices.setRefreshTokenValiditySeconds(1);
	}*/
}
