package com.app.controllers.entrevue;

import com.app.model.entrevue.Entrevue;
import com.app.model.entrevue.EntrevueTO;
import com.app.model.users.User;
import com.app.services.entrevue.EntrevueService;
import com.app.services.users.UserService;
import com.app.utils.Constants;
import com.app.utils.Data;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value =  Constants.URL_ENTREVUE)
public class EntrevueController {
	@Autowired
	private  EntrevueService entrevueService;
	@Autowired
	private  UserService userService;
	
	private static final Logger LOG = Logger.getLogger(EntrevueController.class);

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.POST)
    public Data createEntrevue(@RequestBody EntrevueTO entrevueTO) throws IOException {
    	try {
    		final ModelMapper mapper = new ModelMapper();
			final User user = userService.getUserById(entrevueTO.getUserId());
			final Entrevue entrevue = entrevueService.createEntrevue(entrevueTO.getEntrevue(), entrevueTO.getTitle(), user);
			final EntrevueTO result = mapper.map( entrevue, EntrevueTO.class);
			return Data.ok(result);
		} catch (Exception e) {
			LOG.error("An error occured while creating.",e);
			return Data.internalServerError(e);
		}
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{entrevueId}", method = RequestMethod.GET)
    public Data getEntrevueById(@PathVariable("entrevueId") Long entrevueId) {
    	try {
    		final ModelMapper mapper = new ModelMapper();
			final Entrevue entrevue = entrevueService.getEntrevueById(entrevueId);
			final EntrevueTO result = mapper.map( entrevue, EntrevueTO.class);
			return Data.ok(result);
		} catch (Exception e) {
			LOG.error("An error occured while getting annotations.",e);
			return Data.internalServerError(e);
		}
       
    }

    @SuppressWarnings("rawtypes")
	@PostMapping("/")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public Data getAllEntrevues() {
    	final ModelMapper mapper = new ModelMapper();
    	try {
    		List<Entrevue> entrevues = entrevueService.getEntrevues();
			if (entrevues != null) {
				List<EntrevueTO> entrevuesTO = entrevues.stream().map(entrevue -> mapper.map(entrevue, EntrevueTO.class))
						.collect(Collectors.toList());
				return Data.found(entrevuesTO);
			} else {
				return Data.notFound(Constants.REQUEST_EMPTY_RESULT);
			}
		} catch (Exception e) {
			LOG.error("An error occured while getting annotations.",e);
			return Data.internalServerError(e);
		}

    }
    @SuppressWarnings("rawtypes")
   	@RequestMapping(value = "/delete/{entrevueId}", method = RequestMethod.DELETE)
       public Data deleteId(@PathVariable("entrevueId") Long entrevueId) {
       	try {
       		final Boolean deleted = entrevueService.deleteEntrevue(entrevueId);
   			return Data.ok(deleted);
   		} catch (Exception e) {
   			LOG.error("An error occured while getting annotations.",e);
   			return Data.internalServerError(e);
   		}
          
   }
}
