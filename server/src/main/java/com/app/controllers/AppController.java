/**
 * 
 */
package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Application Entry Point 
 * @author hfoko
 *
 */
@Controller
public class AppController {
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home() {
		return "/index.html";
	}

}
