/**
 * 
 */
package com.app.utils;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author hfoko
 *
 */
public final class DateTimeSerializer extends JsonSerializer<DateTime> {

	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
	private static final DateTimeFormatter ALT_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");

	public DateTimeSerializer() {
	}

	@Override
	public void serialize(DateTime value, JsonGenerator generator, SerializerProvider arg2) throws IOException {
		if (value != null) {
			try {
				generator.writeString(FORMATTER.print(value));
			} catch (Exception e) {
				generator.writeString(ALT_FORMATTER.print(value));
			}
		}
	}

}
