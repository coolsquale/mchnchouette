/**
 * 
 */
package com.app.utils;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * @author hfoko
 *
 */
public final class DateTimeDeSerializer extends JsonDeserializer<DateTime> {

	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
	private static final DateTimeFormatter ALT_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");

	public DateTimeDeSerializer() {
	}

	@Override
	public DateTime deserialize(JsonParser parser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		if (parser.getText() != null) {
			try {
				return FORMATTER.parseDateTime(parser.getText());
			} catch (Exception e) {
				return ALT_FORMATTER.parseDateTime(parser.getText());
			}
		} else {
			return null;
		}
	}

}
