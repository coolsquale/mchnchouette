/**
 * 
 */
package com.app.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hfoko
 *
 */
public final class Constants {
	
	// REST End Points
	public static final String APP_URL = "http://localhost:8080";
	public static final String URL_USERS = "/users";
	public static final String URL_ANNOTATION = "/annotations";
	public static final String URL_ENTREVUE = "/entrevue";
	public static final String URL_UNITEH = "/unitehemeneutique";
	public static final String URL_LOGIN = "/login";
	public static final String URL_LOGOUT = "/logout";
	public static final String SWAGGER_UI = "/swagger-ui.html";
	public static final String DEFAULT_USER = "/defaultuser";
	public static final String SIGN_UP = "/signup";
	public static final String RESET_PASSWORD = "/resetpassword";

	// REST API Service Level Message
	public static final String USER_INVALID_EMAIL = "Email is invalid or already taken";
	public static final String NOT_FOUND_EXCEPTION = "An error occured while retrieving, the requested data does not exist or have been deleted.";
	public static final String PASSWORD_NOT_MATCHING = "Old and new password must be different";
	public static final String CURRENT_PASSWORD_NOT_MATCHING = "The current password doen't match our record";
	public static final String ENTITY_NOT_FOUND = "Entity not found.";
	public static final String NOTIFICATION_ERROR = "An error occured while sending notifications to users";
	public static final String GOOGLE_API_ADDRESS_REQUEST = "An error occured while requeting address from Google API";
	public static final String GOOGLE_API_ADDRESS_DO_NOT_EXIST = "Address does not exist";
	public static final String USER_INFO_ERROR_CREATION = "An error occured during user creation.";
	public static final String USER_ERROR_CONFIRMATION = "An error occured during user confirmation.";
	public static final String USER_ERROR_UPDATE_PASSWORD = "An error occured during password update.";
	public static final String USER_ERROR_UPDATE = "An error occured while updating user";
	public static final String UNKNOWN_ERROR = "An unknown error occured";
	public static final String EMAIL_NOTIFICATION_ERROR = "An error occured while sending email notification";
	public static final String SAVING_MESSAGE_ERROR = "An error occured while saving user message";
	public static final String GETTING_MESSAGE_ERROR = "An error occured while getting user message";
	public static final String GETTING_CONVERSATION_ERROR = "An error occured while fetching user's conversation";
	public static final String DELETING_MESSAGE_ERROR = "An error occured while deleting user's message";
	public static final String GETTING_ACTIVITIES_ERROR = "An error occured while fetching user's activities";
	public static final String UNSUSCRIBE_ERROR = "An error occured while completing the unsuscribe process";
	public static final String NOTIFICATION_PATH_LOCATION_ERROR = "An error occured while fetching notification data";
	public static final String SENDING_NOTIFICATION_ERROR = "An error occured while sending notification data";
	public static final String GETTING_DATA_ASSOCIATION_ERROR = "An error occured while getting associated data";
	public static final String REQUEST_CREATION_ERROR = "An error occured while creating the new request";
	public static final String REQUEST_PROCESSING_ERROR = "An error occured while processing the request";
	public static final String SEARCH_PROCESSING_ERROR = "An error occured while processing the search query";
	public static final String SUGGESTION_CREATION_ERROR = "An error occured while generating the ads suggestions";
	public static final String TRANSACTION_CREATION_ERROR = "An error occured while creating the new transaction";
	public static final String TRANSACTION_PROCESSING_ERROR = "An error occured while processing the transaction query";
	public static final String PROCESSING_ERROR = "An unknow error occured while processing the data.";
	
	
	// ERRORS CODE
	public static final Long ERROR_CODE_UNDEFINED = 0L;
	public static final Long ERROR_CODE_TRIP = 1L;
	public static final Long ERROR_CODE_PACKAGE = 2L;
	public static final Long ERROR_CODE_STORE = 3L;
	public static final Long ERROR_CODE_LOCATION = 4L;
	public static final Long ERROR_CODE_MESSAGE = 5L;
	public static final Long ERROR_CODE_NOTIFICATION = 6L;
	public static final Long ERROR_CODE_REQUEST = 7L;
	public static final Long ERROR_CODE_SEARCH = 8L;
	public static final Long ERROR_CODE_SUGGESTION = 9L;
	public static final Long ERROR_CODE_TRANSACTION = 10L;
	public static final Long ERROR_CODE_USER_REVIEW = 11L;
	public static final Long ERROR_CODE_USER = 12L;
	public final static List<Object> REQUEST_EMPTY_RESULT = new ArrayList<>();
	private Constants() {
	}
	public enum Role {
		USER("USER"), ADMIN("ADMIN"), CUSTOMERS("CUSTOMERS");
		
		private final String name;
		
		private Role(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	public enum HttpStatus {
		INTERNAL_SERVER_ERROR(0L, "An unexpected server error occured"), 
		OK(1L, "Ok"), 
		FOUND(2L,"Results were found for GET query"),
		NOT_FOUND(2L,"No Results were found for GET query");
		private final Long id;
		private final String text;

		private HttpStatus(Long id, String text) {
			this.id = id;
			this.text = text;
		}

		public Long getId() {
			return id;
		}

		public String getText() {
			return text;
		}
	}
	
	public enum ServiceExceptionType {
		INFO(0L), WARN(1L), ERROR(2L), UNKNOWN(3L);
		private final Long id;
		private ServiceExceptionType(Long id) {
			this.id = id;
		}
		public Long getId() {
			return id;
		}
	}

	public enum ConnectionType {
		IN, OUT;
	}
	
	public enum SubscriptionType {
		LOCATION, USER, PATH
	}
}
