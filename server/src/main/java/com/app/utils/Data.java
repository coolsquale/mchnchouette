/**
 * 
 */
package com.app.utils;

import java.io.Serializable;

import com.app.utils.Constants.HttpStatus;

/**
 * @author hfoko
 *
 */
public class Data<T> implements Serializable {

	private static final long serialVersionUID = 928893308100821146L;
	private T body;
	private HttpStatus statusCode;

	public Data() {
	}

	public Data(HttpStatus statusCode) {
		this(null, statusCode);
	}

	public Data(T body, HttpStatus statusCode) {
		this.body = body;
		this.statusCode = statusCode;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	@SuppressWarnings("rawtypes")
	public static Data ok() {
		return new Data<>(HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes" })
	public static <T> Data ok(T body) {
		return new Data<>(body, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes" })
	public static <T> Data found(T body) {
		return new Data<>(body, HttpStatus.FOUND);
	}
	
	@SuppressWarnings("rawtypes")
	public static <T> Data notFound() {
		return new Data<>(HttpStatus.NOT_FOUND);
	}
	
	@SuppressWarnings("rawtypes")
	public static <T> Data notFound(T body) {
		return new Data<>(HttpStatus.NOT_FOUND);
	}
	
	@SuppressWarnings("rawtypes")
	public static <T> Data internalServerError(T body) {
		return new Data<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
