/**
 * 
 */
package com.app.dao.users;

import java.util.List;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public interface UserDao {
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	User getUserById(Long id) throws Exception;
	
	/**
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	User getUserByName(String username) throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<User> getUsers() throws Exception;
	
	/**
	 * 
	 * @param user
	 * @throws Exception
	 */
	void createUser(User user) throws Exception;
	
	/**
	 * 
	 * @param user
	 * @throws Exception
	 */
	User updateUser(User user) throws Exception;

	
	/**
	 * 
	 * @throws Exception
	 */
	void updatePassword(Long userId, String password) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Long> getUsersId() throws Exception;
	

}
