/**
 * 
 */
package com.app.dao.users;

import java.sql.Types;
import java.util.List;
import java.util.Map;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import com.app.dao.AppStoredProcedure;
import com.app.model.users.User;
import com.app.model.users.UserRowMapper;

/**
 * @author hfoko
 *
 */
@Repository
public class UserDaoImpl implements UserDao {
	
	//private static final Logger LOG = Logger.getLogger(UserDaoImpl.class);
	private static final String SPROC_USER_SET = "sprocUserSet";

	public static final String GET_BY_ID = "SELECT user_id, date_created,"
			+ " last_name, first_name, '' as password,"
			+ "email FROM user where user_id = ?";

	public static final String GET_BY_NAME = "SELECT user_id, date_created,"
			+ " last_name, first_name, password,"
			+ "email FROM user where email = ?";

	public static final String GET_ALL = "SELECT user_id, date_created "
			+ ", last_name, first_name, '' as password,"
			+ "email FROM user ";
	
	public static final String GET_ALL_IDS = "SELECT user_id FROM user ";

	private static final String UPDATE_PASSWORD = "UPDATE user SET password=? WHERE user_id=?"; 
			
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public UserDaoImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.users.UserDao#getUserById(java.lang.Long)
	 */
	@Override
	public User getUserById(Long id) throws Exception {
		return jdbcTemplate.queryForObject(GET_BY_ID, new Object [] {id}, new UserRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.users.UserDao#getUserByName(java.lang.String)
	 */
	@Override
	public User getUserByName(String username) throws Exception {
		return jdbcTemplate.queryForObject(GET_BY_NAME, new Object [] {username}, new UserRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.users.UserDao#getUsers()
	 */
	@Override
	public List<User> getUsers() throws Exception {
		return jdbcTemplate.query(GET_ALL, new UserRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.users.UserDao#add(com.app.model.users.User)
	 */
	@Override
	public void createUser(User user) throws Exception {
		//Long userId = 
		setUser(user);
	}

	/**
	 * @param user
	 */
	private Long setUser(User user) {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_USER_SET);
		// Sql parameters
		SqlInOutParameter pUserId = new SqlInOutParameter("p_user_id", Types.BIGINT);
		SqlParameter p_first_name = new SqlParameter("p_first_name", Types.VARCHAR);
		SqlParameter p_last_name = new SqlParameter("p_last_name", Types.VARCHAR);
		SqlParameter p_password = new SqlParameter("p_password", Types.VARCHAR);
		SqlParameter p_email = new SqlParameter("p_email", Types.VARCHAR);
		SqlParameter[] parameters = { pUserId, p_first_name, p_last_name, p_password,p_email };
		
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		Map<String,Object> results = storedProcedure.execute(user.getId(), user.getFirstName(), user.getLastName(),
				 user.getPassword(), user.getEmail());
		return (Long) results.get("p_user_id");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.app.dao.users.UserDao#update(com.app.model.users.User)
	 */
	@Override
	public User updateUser(User user) throws Exception {
		setUser(user);
		return user;
	}

	@Override
	public void updatePassword(Long userId, String password) throws Exception {
		jdbcTemplate.update(UPDATE_PASSWORD, new Object [] {password, userId} );
	}


	@Override
	public List<Long> getUsersId() throws Exception {
		return jdbcTemplate.queryForList(GET_ALL_IDS, Long.class);
	}

}
