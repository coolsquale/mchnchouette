/**
 * 
 */
package com.app.dao.paragraph;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import com.app.dao.AppStoredProcedure;
import com.app.model.entrevue.Entrevue;
import com.app.model.paragraph.Paragraph;
import com.app.model.paragraph.ParagraphRowMapper;

/**
 * @author hfoko
 *
 */
@Repository
public class ParagraphDaoImpl implements ParagraphDao {
	
	//private static final Logger LOG = Logger.getLogger(ParagraphDaoImpl.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String SPROC_PARAGRAPH_SET = "sprocParagraphSet";
	private static final String GET_PARAGRAPHS =  "select prgrph_id, paragraph from paragraphs";
	private static final String BY_ID = " where ntrvs_id = ? order by prgrph_id asc";

	@Override
	public List<Paragraph> getParagraphs(Long entrevue_id) throws Exception {
		List<Paragraph> list = new ArrayList<>();
		list = jdbcTemplate.query(GET_PARAGRAPHS + BY_ID, new Object[] {entrevue_id}, new ParagraphRowMapper());
		return list;
	}

	@Override
	public void setParagraphs(Entrevue entrevue) throws Exception {
		for(Paragraph paragraph : entrevue.getParagraphes()){
			setParagraph(entrevue.getId(), paragraph);
		}
	}
	
	private void setParagraph(Long entrevue_id, Paragraph paragraph) throws Exception {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_PARAGRAPH_SET);
		SqlOutParameter p_value_id = new SqlOutParameter("p_value_id", Types.BIGINT);
		SqlParameter p_ntrvs_id = new SqlParameter("p_ntrvs_id", Types.BIGINT);
		SqlParameter p_prgrph_id = new SqlParameter("p_prgrph_id", Types.BIGINT);
		SqlParameter p_paragraph = new SqlParameter("p_paragraph", Types.VARCHAR);
		SqlParameter[] parameters = { p_value_id, p_ntrvs_id, p_prgrph_id, p_paragraph};
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		storedProcedure.execute(entrevue_id, paragraph.getParagraphId(),paragraph.getParagraph());
	}
	
}
