/**
 * 
 */
package com.app.dao.paragraph;

import java.util.List;

import com.app.model.entrevue.Entrevue;
import com.app.model.paragraph.Paragraph;

/**
 * @author hfoko
 *
 */
public interface ParagraphDao {
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Paragraph> getParagraphs(Long entrevue_id) throws Exception;
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	void setParagraphs(Entrevue entrevue) throws Exception;
	
	
}
