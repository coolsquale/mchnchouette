package com.app.dao.entrevue;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import com.app.dao.AppStoredProcedure;
import com.app.model.entrevue.Entrevue;
import com.app.model.entrevue.EntrevueRowMapper;

@Repository
public class EntrevueDaoImpl implements EntrevueDao {
	
	//private static final Logger LOG = Logger.getLogger(EntrevueDaoImpl.class);
	private  static final  String SPROC_ENTREVUE_SET = "sprocEntrevueSet";
	private static final String SPROC_ENTREVUE_DEL = "sprocEntrevueDelete";
	private static final String GET_ENTREVUE = "select e.ntrvs_id, e.title, e.content, e.created_date, u.user_id, u.first_name,u.last_name,"
												+" '' as password, u.email, u.date_created from entrevues e join user u on e.user_id = u.user_id ";
	private static final String BY_ID = " where e.ntrvs_id = ?";
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Entrevue getEntrevueById(Long ntrv_id) throws Exception {
		Entrevue entrevue = jdbcTemplate.queryForObject(GET_ENTREVUE + BY_ID, new Object[] {ntrv_id}, new EntrevueRowMapper());
		return entrevue;
	}

	@Override
	public List<Entrevue> getAllEntrevue() throws Exception {
		List<Entrevue> list = new ArrayList<>();
		list = jdbcTemplate.query(GET_ENTREVUE, new EntrevueRowMapper());
		return list;
	}

	@Override
	public Boolean deleteEntrevue(Long entrevue_id) throws Exception {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_ENTREVUE_DEL);
		SqlParameter p_ntrv_id = new SqlParameter("p_ntrv_id", Types.BIGINT);
		SqlParameter[] parameters = { p_ntrv_id};
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		storedProcedure.execute(entrevue_id);
		return true;
	}

	@Override
	public Entrevue saveEntrevue(Entrevue entrevue) throws Exception {
		return setEntrevue(entrevue);
	}
	
	private Entrevue setEntrevue(Entrevue entrevue){
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_ENTREVUE_SET);
		SqlInOutParameter p_ntrv_id = new SqlInOutParameter("p_ntrvs_id", Types.BIGINT);
		SqlParameter p_user_id = new SqlParameter("p_user_id", Types.BIGINT);
		SqlParameter p_title = new SqlParameter("p_title", Types.VARCHAR);
		SqlParameter p_content = new SqlParameter("p_content", Types.VARCHAR);
		SqlParameter[] parameters = { p_ntrv_id, p_user_id, p_title, p_content};
		
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		Map<String,Object> results = storedProcedure.execute(entrevue.getId(),entrevue.getUser().getId(), entrevue.getTitle(),entrevue.getEntrevue());
		entrevue.setId((Long) results.get("p_ntrvs_id"));
		return entrevue;
	}

}
