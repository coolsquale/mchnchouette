package com.app.dao.entrevue;

import java.util.List;
import com.app.model.entrevue.Entrevue;

/**
 * 
 * @author hfoko
 *
 */
public interface EntrevueDao {

	/**
	 * 
	 * @param userId
	 * @param adType
	 * @return
	 * @throws Exception
	 */
	Entrevue getEntrevueById(Long ntrv_id) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Entrevue> getAllEntrevue() throws Exception;

	/**
	 * 
	 * @param ntrv_id
	 * @throws Exception
	 */
	Boolean deleteEntrevue(Long entrevue_id) throws Exception;

	/**
	 * 
	 * @param entrevue
	 * @return
	 * @throws Exception
	 */
	Entrevue saveEntrevue(Entrevue entrevue) throws Exception;
	
	
}
