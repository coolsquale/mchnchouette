/**
 * 
 */
package com.app.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * @author hfoko
 *
 */
public class AppStoredProcedure extends StoredProcedure {

	public AppStoredProcedure() {
	}

	/**
	 * @param ds
	 * @param name
	 */
	public AppStoredProcedure(DataSource ds, String name) {
		super(ds, name);
		setFunction(false);
	}

	/**
	 * @param jdbcTemplate
	 * @param name
	 */
	public AppStoredProcedure(JdbcTemplate jdbcTemplate, String name) {
		super(jdbcTemplate, name);
		setFunction(false);
	}

}
