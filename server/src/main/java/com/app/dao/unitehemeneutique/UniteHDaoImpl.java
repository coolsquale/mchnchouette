/**
 * 
 */
package com.app.dao.unitehemeneutique;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import com.app.dao.AppStoredProcedure;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.unitehemeneutique.UniteHComment;
import com.app.model.unitehemeneutique.UniteHCommentRowMapper;
import com.app.model.unitehemeneutique.UniteHRowMapper;
import com.app.model.users.User;

/**
 * @author Hfoko
 *
 */
@Repository
public class UniteHDaoImpl implements UniteHDao {

	//private static final Logger LOG = Logger.getLogger(UniteHDaoImpl.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String SPROC_UNITE_SET = "sprocUniteHSet";
	private static final String SPROC_UNITE_DEL = "sprocUniteHDelete";
	private static final String SPROC_COMMENT_SET = "sprocUniteHCommentSet";
	private static final String GET_UNITE_H = "select h.unite_id, h.title, h.created_date, u.user_id, u.first_name, u.last_name, '' as password, u.email, u.date_created " 
													+ "from unite_h h join user u on h.user_id = u.user_id";
	private static final String GET_UNITE_H_COMMENT = "select c.comment_id, c.comment, c.created_date, u.user_id, u.first_name, u.last_name, '' as password, u.email, u.date_created"
	+" from unite_h_comment c join user u on c.user_id = u.user_id";
	private static final String BY_ID = " where unite_id = ?";
	private static final String BY_PARAGRAPH_ID = " where prgrph_id = ?";

	
	@Override
	public UniteH createUniteH(String title, User user) throws Exception {
		return setUniteH(new UniteH(title,user));
	}

	@Override
	public UniteH getUniteHById(Long unite_id) throws Exception {
		UniteH unite = jdbcTemplate.queryForObject(GET_UNITE_H + BY_ID, new Object[] {unite_id}, new UniteHRowMapper());
		return unite;
	}

	@Override
	public Boolean updateUniteH(UniteH unite) throws Exception {
		setUniteH(unite);
		return true;
	}

	@Override
	public Boolean deleteUniteH(UniteH unite) throws Exception {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_UNITE_DEL);
		SqlParameter pUniteId = new SqlParameter("p_unite_id", Types.BIGINT);
		SqlParameter[] parameters = { pUniteId};
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		storedProcedure.execute(unite.getId());
		return true;
	}
	private UniteH setUniteH(UniteH unite) {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_UNITE_SET);
		SqlInOutParameter pUniteId = new SqlInOutParameter("p_unite_id", Types.BIGINT);
		SqlParameter p_title = new SqlParameter("p_title", Types.VARCHAR);
		SqlParameter p_user_id = new SqlParameter("p_user_id", Types.BIGINT);
		SqlParameter[] parameters = { pUniteId, p_title, p_user_id};
		
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		Map<String,Object> results = storedProcedure.execute(unite.getId(), unite.getTitle(),unite.getUser().getId());
		unite.setId((Long) results.get("p_unite_id"));
		return unite;
	}

	@Override
	public List<UniteH> getAllUniteH() throws Exception {
		List<UniteH> list = new ArrayList<>();
		list = jdbcTemplate.query(GET_UNITE_H, new UniteHRowMapper());
		return list;
	}

	@Override
	public List<UniteHComment> getAllUniteHComment(Long unite_id) throws Exception {
		List<UniteHComment> list = new ArrayList<>();
		list = jdbcTemplate.query(GET_UNITE_H_COMMENT + BY_ID, new Object[] {unite_id}, new UniteHCommentRowMapper());
		return list;
	}

	@Override
	public void AddUniteHComment(Long unite_id,UniteHComment comment) throws Exception {
		 setUniteHComment(unite_id, comment);		
	}
	
	private UniteHComment setUniteHComment(Long unite_id, UniteHComment comment) {
		AppStoredProcedure storedProcedure = new AppStoredProcedure(jdbcTemplate, SPROC_COMMENT_SET);
		SqlInOutParameter p_comment_id = new SqlInOutParameter("p_comment_id", Types.BIGINT);
		SqlParameter p_comment = new SqlParameter("p_comment", Types.VARCHAR);
		SqlParameter p_user_id = new SqlParameter("p_user_id", Types.BIGINT);
		SqlParameter pUniteId = new SqlParameter("p_unite_id", Types.BIGINT);
		SqlParameter[] parameters = { p_comment_id,p_comment, p_user_id,pUniteId};
		
		storedProcedure.setParameters(parameters);
		storedProcedure.compile();
		Map<String,Object> results = storedProcedure.execute(comment.getId(), comment.getComment(),comment.getUser().getId(),unite_id );
		comment.setId((Long) results.get("p_comment_id"));
		return comment;
	}

	@Override
	public List<UniteH> getAllUniteH(Long paragraph_id) throws Exception {
		List<UniteH> list = new ArrayList<>();
		list = jdbcTemplate.query(GET_UNITE_H + BY_PARAGRAPH_ID, new Object[] {paragraph_id}, new UniteHRowMapper());
		return list;
	}
}
