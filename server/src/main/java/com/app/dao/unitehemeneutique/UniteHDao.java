/**
 * 
 */
package com.app.dao.unitehemeneutique;

import java.util.List;

import com.app.model.unitehemeneutique.UniteH;
import com.app.model.unitehemeneutique.UniteHComment;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public interface UniteHDao {

	/**
	 * 
	 * @param title TODO
	 * @param user TODO
	 */
	UniteH createUniteH(String title, User user) throws Exception;
	
	/**
	 * 
	 * @param title TODO
	 * @param user TODO
	 */
	List <UniteH> getAllUniteH() throws Exception;
	
	/**
	 * 
	 * @param title TODO
	 * @param user TODO
	 */
	List <UniteH> getAllUniteH(Long paragraph_id) throws Exception;
	
	/**
	 * 
	 * @param title TODO
	 * @param user TODO
	 */
	UniteH getUniteHById(Long unite_id) throws Exception;

	/**
	 * 
	 * @param transactionId
	 * @param transactionStatus
	 * @throws Exception
	 */
	Boolean updateUniteH(UniteH unite) throws Exception;

	/**
	 * 
	 * @param transactionId
	 * @return
	 * @throws Exception
	 */
	Boolean deleteUniteH(UniteH unite) throws Exception;
	
	/**
	 * 
	 * @param transactionId
	 * @return
	 * @throws Exception
	 */
	List <UniteHComment> getAllUniteHComment(Long unite_id) throws Exception;
	
	/**
	 * 
	 * @param transactionId
	 * @return
	 * @throws Exception
	 */
	void AddUniteHComment(Long unite_id,UniteHComment comment) throws Exception;

}
