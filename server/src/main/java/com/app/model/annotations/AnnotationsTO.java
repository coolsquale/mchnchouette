/**
 * 
 */
package com.app.model.annotations;

import java.io.Serializable;
import org.joda.time.DateTime;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.users.User;
import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author hfoko
 *
 */
public class AnnotationsTO implements Serializable {

	private static final long serialVersionUID = -5489656740122567024L;
	private Long id;
	private User user;
	private UniteH unite;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	
	public AnnotationsTO() {
	}
	
	public AnnotationsTO(Long id, UniteH unite, User user, DateTime createdOn) {
		super();
		this.id = id;
		this.user = user;
		this.unite = unite;
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UniteH getUniteH() {
		return unite;
	}

	public void setUser(UniteH unite) {
		this.unite = unite;
	}
	
	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
}
