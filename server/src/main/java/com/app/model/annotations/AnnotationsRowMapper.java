/**
 * 
 */
package com.app.model.annotations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public final class AnnotationsRowMapper implements RowMapper<Annotations> {

	@Override
	public Annotations mapRow(ResultSet rs, int rownum) throws SQLException {
		Date dateCreated = rs.getDate("date_created_u");
		User user = new User(rs.getLong("user_id"), rs.getString("first_name"), rs.getString("last_name"),
				rs.getString("password"),rs.getString("email"),new DateTime(dateCreated.getTime()));
		dateCreated = rs.getDate("date_created_h");
		UniteH unite = new UniteH(rs.getLong("unite_id"), rs.getString("title"), user, new DateTime(dateCreated.getTime()));
		Annotations annotation = new Annotations(rs.getLong("annotation_id"), unite, user, new DateTime(dateCreated.getTime()));
		return annotation;	
	}

}
