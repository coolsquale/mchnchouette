/**
 * 
 */
package com.app.model.annotations;

import org.joda.time.DateTime;
import com.app.model.unitehemeneutique.UniteH;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class Annotations {

	private Long id;
	private User user;
	private UniteH unite;
	private DateTime createdOn;
	
	public Annotations() {
	}
	public Annotations(UniteH unite, User user) {
		super();
		this.user = user;
		this.unite = unite;
	}
	public Annotations(Long id, UniteH unite, User user, DateTime createdOn) {
		super();
		this.id = id;
		this.user = user;
		this.unite = unite;
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UniteH getUniteH() {
		return unite;
	}

	public void setUser(UniteH unite) {
		this.unite = unite;
	}
	
	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
}
