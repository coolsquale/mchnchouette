/**
 * 
 */
package com.app.model.paragraph;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author hfoko
 *
 */
public class ParagraphRowMapper implements RowMapper<Paragraph> {

	@Override
	public Paragraph mapRow(ResultSet rs, int row) throws SQLException {
		Paragraph paragraph = new Paragraph(rs.getLong("prgrph_id"), rs.getString("paragraph"));
		return paragraph;	
	}

}
