/**
 * 
 */
package com.app.model.paragraph;

import java.util.ArrayList;
import com.app.model.annotations.Annotations;

/**
 * @author hfoko
 *
 */
public class Paragraph {
	
	private Long getParagraphId;
	private String paragraph;
	private ArrayList <Annotations> annotations;
	public Paragraph() {
	}
	
	public Paragraph(Long getParagraphId, String paragraph) {
		super();
		this.getParagraphId = getParagraphId;
		this.paragraph = paragraph;
		
	}

	public Long getParagraphId() {
		return getParagraphId;
	}

	public void setParagraphId(Long getParagraphId) {
		this.getParagraphId = getParagraphId;
	}

	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = paragraph;
	}
	
	
	public ArrayList<Annotations> getAnnotation() {
		return annotations;
	}

	public void setAnnotation(ArrayList <Annotations> annotations) {
		this.annotations = annotations;
	}
	
}
