/**
 * 
 */
package com.app.model.unitehemeneutique;

import java.io.Serializable;
import java.util.List;

import org.joda.time.DateTime;

import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author root
 *
 */
public class UniteHTO implements Serializable {

	private static final long serialVersionUID = -1221810008151599934L;
	private Long id;
	private String title;
	private Long userId;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	private List<UniteHCommentTO> comments;
	
	public UniteHTO() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	public void setComments(List<UniteHCommentTO> comments) {
		this.comments = comments;
	}
	
	public List<UniteHCommentTO> getComments() {
		return comments;
	}

}
