/**
 * 
 */
package com.app.model.unitehemeneutique;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class UniteHCommentRowMapper implements RowMapper<UniteHComment> {

	@Override
	public UniteHComment mapRow(ResultSet rs, int rowNum) throws SQLException {

		Date dateCreated = rs.getTimestamp("date_created");
		User user = new User(rs.getLong("user_id"), rs.getString("first_name"), rs.getString("last_name"),
				rs.getString("password"),rs.getString("email"),new DateTime(dateCreated.getTime()));
		dateCreated = rs.getTimestamp("created_date");
		UniteHComment comment = new UniteHComment(rs.getLong("comment_id"), rs.getString("comment"), user, new DateTime(dateCreated.getTime()));
		return comment;
	}

}
