/**
 * 
 */
package com.app.model.unitehemeneutique;

import java.util.List;
import org.joda.time.DateTime;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class UniteH {
	private Long id;
	private String title;
	private User user;
	private DateTime createdOn;
	private List<UniteHComment> comments;
	
	public UniteH() {
	}
	public UniteH(String title, User user) {
		super();
		this.title = title;
		this.user = user;
	}
	public UniteH(Long id, String title, User user, DateTime createdOn) {
		super();
		this.id = id;
		this.title = title;
		this.user = user;
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	public void setComments(List<UniteHComment> comments) {
		this.comments = comments;
	}
	
	public List<UniteHComment> getComments() {
		return comments;
	}

}
