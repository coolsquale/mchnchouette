/**
 * 
 */
package com.app.model.unitehemeneutique;

import java.io.Serializable;
import org.joda.time.DateTime;

import com.app.model.users.UserTO;
import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author root
 *
 */
public class UniteHCommentTO implements Serializable {

	private static final long serialVersionUID = 2687600660211584210L;
	private Long id;
	private String comment;
	private UserTO user;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	
	public UniteHCommentTO() {
	}
	
	public UniteHCommentTO(Long id, String comment, UserTO user, DateTime createdOn) {
		super();
		this.id = id;
		this.comment = comment;
		this.user = user;
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserTO getUser() {
		return user;
	}

	public void setUser(UserTO user) {
		this.user = user;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}

}
