/**
 * 
 */
package com.app.model.unitehemeneutique;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class UniteHRowMapper implements RowMapper<UniteH> {

	@Override
	public UniteH mapRow(ResultSet rs, int rowNum) throws SQLException {

		Date dateCreated = rs.getTimestamp("date_created");
		User user = new User(rs.getLong("user_id"), rs.getString("first_name"), rs.getString("last_name"),
				rs.getString("password"),rs.getString("email"),new DateTime(dateCreated.getTime()));
		dateCreated = rs.getTimestamp("created_date");
		UniteH unite = new UniteH(rs.getLong("unite_id"), rs.getString("title"), user, new DateTime(dateCreated.getTime()));
		return unite;
	}

}
