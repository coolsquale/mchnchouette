/**
 * 
 */
package com.app.model.unitehemeneutique;

import org.joda.time.DateTime;

import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class UniteHComment {
	private Long id;
	private String comment;
	private User user;
	private DateTime createdOn;
	
	public UniteHComment() {
	}
	public UniteHComment(String comment, User user) {
		super();
		this.comment = comment;
		this.user = user;
	}
	public UniteHComment(Long id, String comment, User user, DateTime createdOn) {
		super();
		this.id = id;
		this.comment = comment;
		this.user = user;
		this.createdOn = createdOn;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}

}
