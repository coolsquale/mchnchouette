/**
 * 
 */
package com.app.model.users;

import com.app.utils.Constants.Role;

/**
 * @author nlengc
 *
 */
public class Groups {

	private Long groupId;
	private String name;
	private Role role;
	private String description;
	
	public Groups() {
	}

	public Groups(Long groupId, String name, Role role, String description) {
		this.groupId = groupId;
		this.name = name;
		this.role = role;
		this.description = description;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
