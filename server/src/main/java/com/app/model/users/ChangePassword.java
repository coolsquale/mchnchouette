/**
 * 
 */
package com.app.model.users;

import java.io.Serializable;

/**
 * @author nlengc
 *
 */
public class ChangePassword implements Serializable {

	private static final long serialVersionUID = -6328414242463806622L;
	private Long userId;
	private String password;
	private String newPassword;

	public ChangePassword() {
	}
	
	public ChangePassword(Long userId, String password, String newPassword)  {
		this.userId = userId;
		this.password = password;
		this.newPassword = newPassword;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
