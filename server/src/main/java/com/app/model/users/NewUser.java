/**
 * 
 */
package com.app.model.users;

import java.io.Serializable;

/**
 * @author hfoko
 *
 */
public class NewUser implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String confirmpassword;

	public NewUser() {
	}
	
	public NewUser(String firstname, String lastname, String email, String password, String confirmpassword) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.confirmpassword = confirmpassword;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
	
}