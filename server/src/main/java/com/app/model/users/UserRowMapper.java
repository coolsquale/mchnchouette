/**
 * 
 */
package com.app.model.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author hfoko
 *
 */
public class UserRowMapper implements RowMapper<User> {

	public UserRowMapper() {
	}

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		Date dateCreated = rs.getTimestamp("date_created");
		User user = new User(rs.getLong("user_id"), rs.getString("first_name"), rs.getString("last_name"),
				rs.getString("password"),rs.getString("email"),new DateTime(dateCreated.getTime()));

		return user;
	}

}
