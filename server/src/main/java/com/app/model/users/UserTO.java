/**
 * 
 */
package com.app.model.users;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author nlengc
 *
 */
public class UserTO implements Serializable {

	private static final long serialVersionUID = 773992986314295062L;
	private Long id;
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	private List<GroupsTO> groups = new ArrayList<>();
	
	public UserTO() {
	}
	
	public UserTO(Long id, String firstName, String lastName, String password,
			String email, DateTime createdOn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdOn = createdOn;
		this.email = email;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	public List<GroupsTO> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupsTO> groups) {
		this.groups = groups;
	}
}
