/**
 * 
 */
package com.app.model.users;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

/**
 * @author hfoko
 *
 */
public class User {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private DateTime createdOn;
	private List<Groups> groups = new ArrayList<>();
	
	public User() {
	}
	
	public User(Long id, String firstName, String lastName, String password,
			String email, DateTime createdOn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdOn = createdOn;
		this.email = email;
	}
	public User(String firstName, String lastName, String password,
			String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}
	public List<Groups> getGroups() {
		return groups;
	}

	public void setGroups(List<Groups> groups) {
		this.groups = groups;
	}

}
