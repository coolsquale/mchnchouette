package com.app.model.entrevue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTime;
import com.app.model.paragraph.Paragraph;
import com.app.model.users.User;

public class Entrevue {
	
	private Long id;
	private User user;
	private List <Paragraph> paragraphes = new ArrayList<Paragraph>();
	private String entrevue;
	private String title;
	private DateTime createdOn;
	private static final String PARAGRAPH_SPLIT_REGEX = "\\r\\n|\\n|\\r";
	
	public Entrevue() {
	}
	public Entrevue(String entrevue,String title, User user) {
		super();
		this.user = user;
		this.entrevue = entrevue;
		this.title = title;
		setParagraphes(entrevue);
	}
	public Entrevue(Long id, String entrevue,String title, User user, DateTime createdOn) {
		super();
		this.id = id;
		this.user = user;
		this.entrevue = entrevue;
		this.title = title;
		this.createdOn = createdOn;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Paragraph> getParagraphes() {
		return this.paragraphes;
	}
	
	public void setParagraphes(String entrevue) {
		//String[] paragraphs = entrevue.split(PARAGRAPH_SPLIT_REGEX);
		List<String> list = new ArrayList<String>(Arrays.asList(entrevue.split(PARAGRAPH_SPLIT_REGEX)));
		list.removeAll(Arrays.asList("", null));
		for (int i = 0; i < list.size(); i++) {
			this.paragraphes.add(new Paragraph(new Long(i),list.get(i)));
	    }
		
	}
	
	public void setParagraphes(List<Paragraph> paragraphes) {
		this.paragraphes = paragraphes;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String entrevue) {
		this.entrevue = entrevue;
	}
	public String getEntrevue() {
		return entrevue;
	}

	public void setEntrevue(String entrevue) {
		this.entrevue = entrevue;
	}

}
