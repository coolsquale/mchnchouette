/**
 * 
 */
package com.app.model.entrevue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
public class EntrevueRowMapper implements RowMapper<Entrevue> {

	public EntrevueRowMapper() {
	}

	@Override
	public Entrevue mapRow(ResultSet rs, int arg1) throws SQLException {
		Date dateCreated = rs.getTimestamp("date_created");
		User user = new User(rs.getLong("user_id"), rs.getString("first_name"), rs.getString("last_name"),
				rs.getString("password"),rs.getString("email"),new DateTime(dateCreated.getTime()));
		dateCreated = rs.getTimestamp("date_created");
		Entrevue entrevue = new Entrevue(rs.getLong("ntrvs_id"), rs.getString("content"),rs.getString("title"),user, new DateTime(dateCreated.getTime()));
		
		return entrevue;
		
	}

}
