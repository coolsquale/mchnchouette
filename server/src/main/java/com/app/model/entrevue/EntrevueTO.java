/**
 * 
 */
package com.app.model.entrevue;

import java.io.Serializable;
import java.util.ArrayList;

import org.joda.time.DateTime;

import com.app.api.model.IParagraph;
import com.app.utils.DateTimeDeSerializer;
import com.app.utils.DateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author hfoko
 *
 */
public class EntrevueTO implements Serializable {
	
	private static final long serialVersionUID = 8848874685658691549L;
	private Long id;
	private Long userId;
	private ArrayList <IParagraph> paragraphes = new ArrayList<IParagraph>();
	private String entrevue;
	private String title;
	@JsonDeserialize(using = DateTimeDeSerializer.class)
	@JsonSerialize(using = DateTimeSerializer.class)
	private DateTime createdOn;
	
	public EntrevueTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ArrayList<IParagraph> getParagraphes() {
		return this.paragraphes;
	}
	
	
	public void setParagraphes(ArrayList<IParagraph> paragraphes) {
		this.paragraphes = paragraphes;
	}

	public DateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(DateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getEntrevue() {
		return entrevue;
	}

	public void setEntrevue(String entrevue) {
		this.entrevue = entrevue;
	}

}
