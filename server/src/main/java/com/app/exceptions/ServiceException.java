/**
 * 
 */
package com.app.exceptions;

import java.io.Serializable;

import com.app.utils.Constants.ServiceExceptionType;

/**
 * @author hfoko
 *
 */
public class ServiceException extends Exception implements Serializable {

	private static final long serialVersionUID = 622854083879468600L;

	private ServiceExceptionType serviceExceptionType;
	private Long code;

	public ServiceException() {
	}

	public ServiceException(ServiceExceptionType serviceExceptionType, String message) {
		super(message);
		this.serviceExceptionType = serviceExceptionType;
	}

	public ServiceException(ServiceExceptionType serviceExceptionType, Long code, String message) {
		this(serviceExceptionType, message);
		this.code = code;
	}

	public ServiceExceptionType getServiceExceptionType() {
		return serviceExceptionType;
	}

	public void setServiceExceptionType(ServiceExceptionType serviceExceptionType) {
		this.serviceExceptionType = serviceExceptionType;
	}

	public final Long getCode() {
		return code;
	}

	public final void setCode(Long code) {
		this.code = code;
	}

}
