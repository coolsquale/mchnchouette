/**
 * 
 */
package com.app.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author hfoko
 *
 */
@Configuration
@ConfigurationProperties(prefix = "appConfiguration")
public class AppConfiguration {

	private String frontEndUrl;
	private String backEndUrl;
	private boolean enableSwagger = true;

	public AppConfiguration() {
	}

	public final String getFrontEndUrl() {
		return frontEndUrl;
	}

	public final void setFrontEndUrl(String frontEndUrl) {
		this.frontEndUrl = frontEndUrl;
	}

	public final String getBackEndUrl() {
		return backEndUrl;
	}

	public final void setBackEndUrl(String backEndUrl) {
		this.backEndUrl = backEndUrl;
	}

	public final boolean isEnableSwagger() {
		return enableSwagger;
	}

	public final void setEnableSwagger(boolean enableSwagger) {
		this.enableSwagger = enableSwagger;
	}

}
