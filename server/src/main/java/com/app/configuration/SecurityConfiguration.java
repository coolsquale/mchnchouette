/**
 * 
 */
package com.app.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author nlengc
 *
 */
@Configuration
@ConfigurationProperties(prefix = "securityConfiguration")
public class SecurityConfiguration {
	
	public static final String RESOURCE_ID = "app";
	public static final String SECURITY_SCOPE_READ = "read";
	public static final String SECURITY_SCOPE_WRITE = "write";
	public static final String AUTHORIZED_GRANT_TYPES_PASSWORD = "password";
	public static final String AUTHORIZED_GRANT_TYPES_REFRESH_TOKEN = "refresh_token";
	public static final String AUTHORIZED_GRANT_TYPES_IMPLICIT = "implicit";
	public static final String AUTHORIZED_GRANT_TYPES_AUTHORIZATION_CODE = "authorization_code";
	
	private String keyStore;
	private String keyStorePassword;
	private String keyPassword;
	private String secret;
	private String clientId;

	public SecurityConfiguration() {
	}

	@Bean(name="passwordEncoder")
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public String getKeyStore() {
		return keyStore;
	}

	public void setKeyStore(String keyStore) {
		this.keyStore = keyStore;
	}

	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	public String getKeyPassword() {
		return keyPassword;
	}

	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}
