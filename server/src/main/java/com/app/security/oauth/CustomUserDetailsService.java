/**
 * 
 */
package com.app.security.oauth;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.app.dao.users.UserDao;
import com.app.dao.users.UserDaoImpl;
import com.app.model.users.User;

/**
 * @author hfoko
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	private static final Logger LOG = Logger.getLogger(UserDaoImpl.class);
	private final UserDao userDao;
	
	@Autowired
	public CustomUserDetailsService(UserDao userDao) {
		this.userDao = userDao;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		try {
			user = userDao.getUserByName(username);
		} catch (Exception e) {
			LOG.error("An error occured during authentification", e);
		}
		if (user == null) {
			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
		}
		return new UserRepositoryUserDetails(user);
	}

	/**
	 * 
	 * @author hfoko
	 *
	 */
	private final static class UserRepositoryUserDetails implements UserDetails {

		private static final long serialVersionUID = 1L;
		//private final Long id;
		private final String password;
		private final String username;
		private final Set<Role> roles;

		private UserRepositoryUserDetails(User user) {
			//this.id = user.getId();
			this.password = user.getPassword();
			this.username = user.getEmail();
			roles = user.getGroups().stream().map(group -> new Role(group.getGroupId(),group.getRole().getName())).collect(Collectors.toSet());
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return this.roles;
		}

		@Override
		public String getUsername() {
			return this.username;
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

		@Override
		public String getPassword() {
			return this.password;
		}

	}

}
