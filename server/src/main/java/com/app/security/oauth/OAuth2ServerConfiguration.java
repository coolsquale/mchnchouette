/**
 * 
 */
package com.app.security.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

//import com.app.configuration.AppConfiguration;
import com.app.configuration.SecurityConfiguration;
import com.app.utils.Constants;

/**
 * @author hfoko
 *
 */
@Configuration
public class OAuth2ServerConfiguration {
	
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
		
		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(SecurityConfiguration.RESOURCE_ID).stateless(false);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
			.antMatchers("/", "/home", 
					Constants.URL_ANNOTATION + "/**",Constants.SWAGGER_UI, Constants.URL_LOGIN, Constants.URL_LOGOUT,
					"/oauth/check_token").permitAll()
			.antMatchers(HttpMethod.GET,  Constants.URL_ENTREVUE+"/**").permitAll()
			.antMatchers(Constants.URL_ENTREVUE+"/**").fullyAuthenticated()
			.antMatchers(HttpMethod.GET,  Constants.URL_UNITEH+"/**").permitAll()
			.antMatchers(Constants.URL_UNITEH+"/**").fullyAuthenticated()
			.antMatchers(HttpMethod.POST, Constants.URL_USERS + "/**").permitAll()
			.antMatchers(HttpMethod.GET, Constants.URL_USERS + "/**").permitAll()
			.antMatchers(Constants.URL_USERS).hasRole(Constants.Role.ADMIN.getName())
			.antMatchers(HttpMethod.PUT,Constants.URL_USERS + "/**").authenticated()
			.and().rememberMe()
            .and().logout().logoutRequestMatcher(new AntPathRequestMatcher(Constants.URL_LOGOUT)).invalidateHttpSession(true).deleteCookies("JSESSIONID").logoutSuccessUrl("/home")
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
		
	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		private TokenStore tokenStore = new InMemoryTokenStore();
		private static final int ACCESS_TOKEN_VALIDITY_TIME = 3600;
		private static final int REFRESH_TOKEN_VALIDITY_TIME = 3600;

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		@Autowired
		private CustomUserDetailsService userDetailsService;
		
		//@Autowired
		//private AppConfiguration appConfiguration;
		
		@Autowired
		private SecurityConfiguration securityConfiguration;
		
		@Bean
		public FilterRegistrationBean corsFilter() {
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			CorsConfiguration config = new CorsConfiguration();
			config.setAllowCredentials(true);
			config.addAllowedOrigin("*");
			config.addAllowedHeader("*");
			config.addAllowedMethod("*");
			source.registerCorsConfiguration("/**", config);
			FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
			bean.setOrder(0);
			return bean;
		}
		
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			endpoints.tokenStore(this.tokenStore).authenticationManager(this.authenticationManager)
					.userDetailsService(userDetailsService);
		}
		 
		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.inMemory()
					.withClient(securityConfiguration.getClientId())
					.authorizedGrantTypes(SecurityConfiguration.AUTHORIZED_GRANT_TYPES_PASSWORD,
							SecurityConfiguration.AUTHORIZED_GRANT_TYPES_REFRESH_TOKEN,
							SecurityConfiguration.AUTHORIZED_GRANT_TYPES_IMPLICIT,
							SecurityConfiguration.AUTHORIZED_GRANT_TYPES_AUTHORIZATION_CODE)
					.authorities(Constants.Role.USER.getName())
					.scopes(SecurityConfiguration.SECURITY_SCOPE_READ, SecurityConfiguration.SECURITY_SCOPE_WRITE)
					.accessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_TIME)
					.refreshTokenValiditySeconds(REFRESH_TOKEN_VALIDITY_TIME)
					.resourceIds(SecurityConfiguration.RESOURCE_ID).secret(securityConfiguration.getSecret());
		}
		
		@Override
		public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		    oauthServer.checkTokenAccess("permitAll()");
		}

		@Bean
		@Primary
		public DefaultTokenServices tokenServices() {
			DefaultTokenServices tokenServices = new DefaultTokenServices();
			tokenServices.setSupportRefreshToken(true);
			tokenServices.setTokenStore(this.tokenStore);
			return tokenServices;
		}
		
	}

}
