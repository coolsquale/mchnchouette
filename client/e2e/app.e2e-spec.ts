import { MigrationAppPage } from './app.po';

describe('migration-app App', function() {
  let page: MigrationAppPage;

  beforeEach(() => {
    page = new MigrationAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
