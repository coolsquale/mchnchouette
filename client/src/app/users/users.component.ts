import { Component, OnInit } from '@angular/core';
import { IUser } from '../common/user.interface'
import { UsersService } from './users.service'
import { ACTIVITY_TYPE } from '../utils/constants'
import { Router } from '@angular/router'
@Component( {
    selector: 'users',
    templateUrl: 'users.component.html',
})
export class UsersComponent implements OnInit {

    constructor( private _usersService: UsersService, private _router: Router ) {
    }

    ngOnInit() {
    }

}

