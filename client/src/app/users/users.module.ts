import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { UsersComponent } from './users.component'
import { UsersDashboardComponent } from './users-dashboard.component'
import { UsersProfileComponent } from './users-profile.component'
import { UsersSettingsComponent } from './users-settings.component'
import {UsersUpdateComponent} from './users-update.component'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { usersRouting } from './users.routing'
import { UsersService } from './users.service'
import { SharedModule } from '../utils/shared.module';
import { AgmCoreModule } from '@agm/core';
import { MatDialogModule, MatTabsModule, NoConflictStyleCompatibilityMode } from '@angular/material';

@NgModule(
    {
        declarations: [
            UsersComponent,
            UsersDashboardComponent,
            UsersProfileComponent,
            UsersSettingsComponent,
            UsersUpdateComponent
        ],
        imports: [
            CommonModule,
            FormsModule,
            usersRouting,
            ReactiveFormsModule,
            SharedModule,
            AgmCoreModule, MatDialogModule, MatTabsModule, NoConflictStyleCompatibilityMode
        ],
        exports: [UsersComponent],
        providers: [UsersService]
    }
)
export class UsersModule {
}