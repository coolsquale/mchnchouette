import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { IUser } from '../common/user.interface'
import { UsersService } from './users.service'
import { Router, ActivatedRoute } from '@angular/router'
declare var System: any;
@Component(
    {
        selector: 'profil',
        templateUrl: './users-profile.component.html'
    }
)
export class UsersProfileComponent implements OnInit {
    public user: IUser;
    public errorMessage:string; 
 public errorCode:number;
    constructor(
        private _usersService: UsersService,
        private _router: Router ) {
    }

    ngOnInit() {
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let id: number = currentUser.userId;
            this._usersService.getUserById( id ).subscribe( u => this.user = u, 
                    error => {
                    //this.errorMessage=data.body.message;
                    //System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                    this._router.navigate( ['/login'] );
                    }, () => {});
        } else {
            this._router.navigate( ['/login'] );
        }
    }
    updateUser( event ) {
        this.user = event;
    }

}