/**
 * New typescript file
 */
import { Component, ViewEncapsulation, OnInit } from '@angular/core'
import { UsersService } from './users.service'
import { AuthenticationService } from '../login/authentification.service';
import { Router } from '@angular/router'
import { IUser } from '../common/user.interface'
import { PasswordValidation } from '../utils/validation.component';
import { TranslateService } from '../utils';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
declare var System: any;
@Component(
    {
        templateUrl: './users-settings.component.html',
        encapsulation: ViewEncapsulation.Emulated
    }
)
export class UsersSettingsComponent implements OnInit {
    public pwrdForm: FormGroup;
    public password: FormControl;
    public newpassword: FormControl;
    public confirmpassword: FormControl;
    public notificationForm: FormGroup;
    public allowNotifications: FormControl;
    public updated: boolean = false;
    public valid:boolean = true;
    public errorMessage:string; 
 public errorCode:number;
    onSelect( user: IUser ): void {
    }

    constructor(private _authenticationService: AuthenticationService,
            private _fb: FormBuilder, private _userService: UsersService, private _router: Router )
    { }

    ngOnInit() {
        // init password form
        this.password = new FormControl( '',[Validators.required]  );
        this.newpassword = new FormControl( '', [Validators.required, Validators.minLength(6)]  );
        this.confirmpassword = new FormControl( '', [Validators.required, Validators.minLength(6)]  );
        this.pwrdForm = this._fb.group( {
            'currentpassword': this.password,
            'password': this.newpassword,
            'confirmpassword': this.confirmpassword
        },{validator: PasswordValidation.MatchPassword});
        
        // init notifications form
        this.allowNotifications = new FormControl('');
        this.notificationForm = this._fb.group( {
            'allowNotifications': this.allowNotifications
        });
    }
    
    updatePassword( valid: boolean ) {
        this.valid = valid;
        if ( !valid ) {
            return;
        }
        
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let id: number = currentUser.userId;
            this._authenticationService.updatepassword( id,this.pwrdForm.controls["currentpassword"].value, this.pwrdForm.controls["password"].value )
            .subscribe( result => { this.updated = result; },
            error => {
                console.log( error );
                //this.errorMessage=data.body.message;
                //System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
            },
            () => {this.pwrdForm.reset();});
        }
    }
    
}