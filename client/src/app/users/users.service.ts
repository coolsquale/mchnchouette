/**
 * New typescript file
 */
import { Injectable } from '@angular/core'
import { IUser } from '../common/user.interface'
import { Data } from '../common/data.interface'
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http'
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import { appConfigs } from '../appConfigs';
import 'rxjs/Rx'

@Injectable()
export class UsersService {

    private apiEndpoint: string;
    private urlAddress = "/address"
    private urlAds = "/ads"
    private urlFile = "/files"
    private baseUrl: string;
    private user: IUser;

    constructor( private _http: Http,private _httpc: HttpClient ) {
        this.baseUrl = appConfigs.system.applicationUrl;
    }

    updateUser( user: IUser ): Observable<IUser> {
        this.apiEndpoint = this.baseUrl + "/users";
        let h = new Headers();
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Authorization', 'Bearer ' + currentUser.access_token );
        return this._http
            .put(
            this.apiEndpoint + "/" + user.id,
            JSON.stringify( user ),
            { headers: h })
            .map(( results: Response ) => { this.user = results.json().body; return this.user; });
    }

    getUserById( id: number ): Observable<IUser> {
        this.apiEndpoint = this.baseUrl + "/users";
                let h = new Headers();
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Access-Control-Allow-Origin', '*' );
        console.log(id);
        return this._http
            .get( `${this.apiEndpoint}/${id}`, { headers: h })
            .map(( results: Response ) => { this.user = mapUser(results);return this.user; })
    }


    private options(): any {
        // create authorization header with jwt token
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let headers = new Headers( { 'Authorization': 'Bearer ' + currentUser.access_token });
            headers.append( 'Content-Type', 'application/json' );
            return new RequestOptions( { headers: headers });
        }
    }

}

function toUser( r: any ): IUser {

    let user = <IUser>( {
        id: r.id,
        firstName: r.firstName,
        lastName: r.lastName,
        password: r.password,
        email: r.email
    });
    return user;
}

function mapUser( response: Response ): IUser {
    return toUser( response.json().body );
}
