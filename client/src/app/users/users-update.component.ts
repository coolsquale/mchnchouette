/**
 * New typescript file
 */

import { Component, Input, Output, OnInit, EventEmitter, AfterViewInit } from '@angular/core'
import { IUser } from '../common/user.interface'
import { UsersService } from './users.service'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    FormControl,
    FormArray,
    Validators
} from '@angular/forms'
import { EmailValidation } from '../utils/validation.component';

@Component(
    {
        templateUrl: './users-update.component.html',
        styles: [`
                 .cadre {
                    border-style: groove;
                     border-top-left-radius:3px;
                  border-top-right-radius:3px;
                  }
                  
               `],
    }
)
export class UsersUpdateComponent implements OnInit, AfterViewInit {
    @Output() userUpdated = new EventEmitter<IUser>();
    public user: IUser;
    public insertForm: FormGroup;
    //user
    public id: FormControl;
    public firstName: FormControl;
    public lastName: FormControl;
    //dateOfBirth: FormControl;
    public password: FormControl;
    public email: FormControl;
    public emailVerif: FormControl;
    public status: FormControl;
    public errorMessage:string; 
    public errorCode:number;

    public valid:boolean = true;
    private usrId: number = 1;
    constructor(
        private _fb: FormBuilder,
        private _router: Router,
        private _usersService: UsersService,
        private _route: ActivatedRoute ) {
    }

    onSubmit( valid: boolean ) {
        this.valid = valid;
        if ( !valid ) {
            return;
        }
        this._usersService.updateUser( this.insertForm.value )
            .subscribe( data => {
                this.user = data;
            },
            error => console.log( error ),
            () => { this._router.navigateByUrl( "users/profil" ); });
    }

    ngOnInit() {
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
        } else {
            this._router.navigate( ['/login'] );
        }

        this.id = new FormControl( '' );
        this.firstName = new FormControl( '' );
        this.lastName = new FormControl( '', [Validators.required] );
        this.password = new FormControl( '' );
        this.email = new FormControl( '', [Validators.required] );
        this.emailVerif = new FormControl( '', [Validators.required] );
        this.insertForm = this._fb.group( {
            'id': this.id,
            'firstName': this.firstName,
            'lastName': this.lastName,
            'password': this.password,
            'email': this.email,
            'emailVerif': this.emailVerif
        }, {validator: EmailValidation.MatchEmail /*your validation method*/} );
    }
    
    
    ngAfterViewInit() {
        let id: number = this.usrId;
        this._usersService
            .getUserById( id ).subscribe(
            data => this.user = data,
            error => { console.log( error ); this._router.navigate( ['/login'] ); },
            () => {
                this.id.setValue( this.user.id );
                this.firstName.setValue( this.user.firstName );
                this.lastName.setValue( this.user.lastName );
                this.password.setValue( this.user.password );
                this.email.setValue( this.user.email );
                this.emailVerif.setValue( this.user.email );

            });
    }


}