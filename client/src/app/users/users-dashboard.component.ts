/**
 * New typescript file
 */
import { Component, ViewEncapsulation, OnInit } from '@angular/core'
import { IUser } from '../common/user.interface'
import { UsersService } from './users.service'
import { Router } from '@angular/router'
import { ERROR_CODES, HTTP_STATUS } from '../utils/constants';
declare var System: any; 
@Component(
    {
        selector: 'users-dashboard',
        templateUrl: './users-dashboard.component.html'
        //encapsulation: ViewEncapsulation.Emulated
    }
)
export class UsersDashboardComponent implements OnInit {
    public user: IUser;
    public currentLang: string;
    public errorMessage:string
    public errorCode:number;
    onSelect( user: IUser ): void {
    }

    constructor( private _usersService: UsersService,private _router: Router ) { 
    }

    ngOnInit() {
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let userId: number = currentUser.userId;

            this._usersService
                .getUserById( userId ).subscribe( u => this.user = u, 
                        error => { console.log( error ); 
                        //this.errorMessage=data.body.message;
                        //System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                        this._router.navigate( ['/login'] ); });
        } else {
            this._router.navigate( ['/login'] );
        }
    }


}