/**
 * New typescript file
 */
import { Routes, RouterModule } from '@angular/router'
import { UsersComponent } from './users.component'
import { UsersDashboardComponent } from './users-dashboard.component'
import { UsersSettingsComponent } from './users-settings.component'
import { UsersProfileComponent } from './users-profile.component'
import { UsersUpdateComponent } from './users-update.component'
import { AuthGuard } from '../common/auth.guard';

const appRoutes: Routes = [
    {
        path: '', component: UsersComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: UsersDashboardComponent },
            { path: 'profil', component: UsersProfileComponent, canActivateChild: [AuthGuard] },
            { path: 'update', component: UsersUpdateComponent, canActivateChild: [AuthGuard] },
            { path: 'settings', component: UsersSettingsComponent, canActivateChild: [AuthGuard] },
        ]
    }
]

export const usersRouting = RouterModule.forChild( appRoutes );