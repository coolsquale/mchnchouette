import { Routes, RouterModule } from '@angular/router';

import {ServicesComponent} from './services.component';

const appRoutes: Routes = [
    { path: '', component: ServicesComponent }
];

export const servicesRouting = RouterModule.forChild(appRoutes);
