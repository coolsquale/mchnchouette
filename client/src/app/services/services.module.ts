import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AgmCoreModule} from '@agm/core';
import {ServicesComponent} from './services.component';
import { servicesRouting } from './services.routing';
import {SharedModule} from '../utils/shared.module';
import {MatTabsModule} from '@angular/material';
@NgModule({
  imports: [ CommonModule, servicesRouting, AgmCoreModule ,SharedModule,MatTabsModule],
  declarations: [ ServicesComponent ]
})
export class ServicesModule {}
