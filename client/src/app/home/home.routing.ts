import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent }
];

export const homeRouting = RouterModule.forChild(appRoutes);
