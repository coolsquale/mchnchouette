import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from './home.component';
import { homeRouting } from './home.routing';
import { EntrevueService } from '../entrevue/entrevue.service'
import {SharedModule} from '../utils/shared.module';
@NgModule({
  imports: [ CommonModule, homeRouting ,SharedModule],
  declarations: [ HomeComponent ],
  providers: [EntrevueService]
})
export class HomeModule {}
