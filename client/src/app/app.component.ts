import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './login/authentification.service';
import { UsersService } from './users/users.service'
import { TranslateService } from './utils';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { PasswordValidation,DateValidation } from './utils/validation.component';
import { DatePipe } from '@angular/common';
import { appConfigs } from './appConfigs';
import { IUser } from './common/user.interface'
import { USER_STATUS, SEARCH_TYPE, ERROR_CODES, HTTP_STATUS } from './utils/constants'
declare var System: any;

import { LocationStrategy } from '@angular/common';
@Component( {
    selector: 'app-root',
    host: {
        '(document:click)': 'handleClick($event)',
    },
    templateUrl: './app.component.html',
    styles: [`
             .suggestion {
                position: absolute;
        z-index: 1;
        background-color: powderblue;
              }
              .selected{
    border:solid #4CAF50 1px;
    float:left; 
    margin:2px;
    padding:2px 15px;
}
.selected a{
    cursor:pointer;
    font-weight:bold;
}
#city li:nth-child(odd) {
    background-color: #ccf;
}
           `],
})
export class AppComponent implements OnInit, AfterViewInit {
    
    public user: IUser;
    public radius: number = 25;
    public connected: boolean = false;
    public loginPage: boolean = false;
    public isLoggedIn: boolean;
    public subscribe: any;
    public searchText: string = "";
    public returnUrl: string;
    public wlogin: boolean = false;
    public signPage: boolean = false;
    public signin: boolean = false;
    public id: string;
    public usrId: number;
    public storeName: string[];
    public LoggedIn = sessionStorage.getItem( 'currentUser' )
    //public adType: IAdType[] = [{'id': -1, 'description':"All"}, {'id': 1, 'description':"Trips"}, {'id': 2,'description':"Packages"}];
    public filteredList = [];
    public elementRef;
    public selected = [];
    public translatedText: string;
    public supportedLangs: any[];
    public currentLang: string;
    public query = '';
    public insertForm: FormGroup;
    public loginForm: FormGroup;
    public firstname: FormControl;
    public lastname: FormControl;
    public email: FormControl;
    public password: FormControl;
    public confirmpassword: FormControl;
    public termsAndConditions: FormControl;
    public valid:boolean = true;
    public username: FormControl;
    public loginpassword: FormControl;
    public rememberMe: FormControl; 
    public validLogin:boolean = true;
    public submitted:boolean = false;
    public currentUserName: string;
    public datePipe:any = new DatePipe("en-US");
    public isPopState = false;
    public emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    public errorMessage:string; 
    public errorCode:number;
    
    constructor( myElement: ElementRef,
        private _route: ActivatedRoute,
        private router: Router,
        private _authenticationService: AuthenticationService,
        private _fb: FormBuilder,
        private _usersService: UsersService,
        private _translate: TranslateService, private locStrat: LocationStrategy ) {

        this.elementRef = myElement;
        router.events.subscribe(( val ) => {
            if ( val instanceof NavigationEnd ) {
                this._authenticationService.loggedIn().subscribe( data => { this.connected = data; }, error => { this.connected = false; }, () => { 
                    let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
                    if(currentUser){
                    let id: number = currentUser.userId;
                    this._usersService
                    .getUserById( id ).subscribe(
                            data => this.user = data,
                            error => { console.log( error ); },
                            () => {});}});
            }
            // see also 
            this.loginPage = false;
            if ( router.url.indexOf( "/login" ) !== -1 ) {
                this.loginPage = true;
                
            }
            this.signPage = false;
            if ( router.url == "/login" ) {
                var login_pop = document.getElementById("soap-popupbox");
                if(login_pop != null){
                    login_pop.click();
                }
            }
            if ( router.url == "/signup" ) {
                var login_pop = document.getElementById("soap-popupbox");
                if(login_pop != null){
                    login_pop.click();
                }
                this.signPage = true;
            }
            if(router.url == "/login/resetpassword"){
                document.getElementById("soap-popupbox").click();
            }
            this.wlogin = false;
            this.signin = false;
        });
    }

    ngOnInit() {
        this.locStrat.onPopState(() => {
            this.isPopState = true;
          });
        this.router.events.subscribe(event => {
            // Scroll to top if accessing a page, not via browser history stack
            if (event instanceof NavigationEnd && !this.isPopState) {
              window.scrollTo(0, 0);
              this.isPopState = false;
            }
            
            // Ensures that isPopState is reset
            if (event instanceof NavigationEnd) {
              this.isPopState = false;
            }
          });
        this.supportedLangs = [
            { display: 'English', value: 'en' },
            { display: 'French', value: 'fr' }
        ];
        this.username = new FormControl( '',[Validators.required]  );
        this.loginpassword = new FormControl( '', [Validators.required]  );
        this.rememberMe = new FormControl('');
        this.loginForm = this._fb.group( {
            'username': this.username,
            'password': this.loginpassword,
            'rememberMe': this.rememberMe
        });
        this.firstname = new FormControl( '' ,[Validators.required] );
        this.lastname = new FormControl( '',[Validators.required]  );
        this.email = new FormControl( '', [Validators.required, Validators.pattern(this.emailPattern)] );
        this.password = new FormControl( '' , [Validators.required, Validators.minLength(6)]);
        this.confirmpassword = new FormControl( '' , [Validators.required, Validators.minLength(6)]);
        this.termsAndConditions = new FormControl( '',[Validators.required]  );
        this.insertForm = this._fb.group( {
            'firstname': this.firstname,
            'lastname': this.lastname,
            'email': this.email,
            'password': this.password,
            'confirmpassword': this.confirmpassword,
            'termsAndConditions': this.termsAndConditions,
        },{validator: PasswordValidation.MatchPassword /*your validation method*/});
        // set current langage
        this.selectLang( 'fr' );
        let opts = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
              };

        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/users';
    }
    
    ngAfterViewInit() {
        let username = localStorage.getItem( 'username' );
        let password = localStorage.getItem( 'password' );
        this.username.setValue( username );
        this.loginpassword.setValue( password );
    }

    logout( event: any ) {
        sessionStorage.clear();
        this.connected = false;
        this._authenticationService.logout().subscribe( data => { this.connected = data; }, 
                error => { this.connected = false; this.router.navigate( ['/'] );}, 
                () => { this.router.navigate( ['/'] ); });
    }

    updateMenu( event ) {
        this.connected = ( event === "true" ) ? true : false;
    }

    hideall() {
        this.wlogin = false;
        this.signin = false;
    }

    ShowLogin() {
        this.wlogin = true;
        this.signin = false;
    }

    login( valid: boolean ) {
        this.validLogin = valid;
        if ( !valid ) {
            return;
        }
        this._authenticationService.loginWithRememberMe( this.loginForm.controls["username"].value, this.loginForm.controls["password"].value, 
                this.loginForm.controls["rememberMe"].value)
            .subscribe( result => { this.isLoggedIn = result; },
            error => {
                this.submitted = true;
                console.log( 'Username or password is incorrect' ); 
                console.log( error );
            },
            () => {
                if ( this.isLoggedIn == true ) {
                    this._authenticationService._getCurrentUser( this.loginForm.controls["username"].value ).subscribe( data => this.id = data,
                        error => console.log( error ),
                        () => {
                            let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
                            if ( currentUser && currentUser.access_token ) {
                                if (currentUser && currentUser.status===USER_STATUS.UNDER_REVIEW) {
                                    this.router.navigate( ['login/activate'] );
                                }
                                let id: number = currentUser.userId;
                                this.currentUserName = this.loginForm.controls["username"].value;
                                this._usersService
                                    .getUserById( id ).subscribe(
                                    data => this.user = data,
                                    error => { console.log( error ); this.router.navigate( ['/login'] ); },
                                    () => {});
                             }
                             this.wlogin = false;
                             this.signin = false;
                             this.loginForm.reset();
                             this.connected = true;
                             document.getElementById( "soap-popupbox" ).click();
                        });

                } else {
                    // redirect to error page, display message (in red) on screen
                }
            });
    }
    
    ShowSignup() {
        this.wlogin = false;
        this.signin = true;
    }

    signup( valid:boolean ) {
         this.valid = valid;
         if(!valid){
             return;
         }
         this._authenticationService.signup( this.insertForm.controls["firstname"].value,this.insertForm.controls["lastname"].value,
                this.insertForm.controls["email"].value, this.insertForm.controls["password"].value, this.insertForm.controls["confirmpassword"].value )
                .subscribe( result => this.router.navigate( ['/home'] ),
                error => {
                    this.errorMessage=error.body.message;
                    System.import('assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                    console.log(error);
                    },
                    ()=>{
                        this.wlogin = false;
                        this.signin = false;
                        document.getElementById("soap-popupbox").click();
                        this.insertForm.reset();
                        System.import('assets/js/theme-scripts.js').then(MyModule=>MyModule.showSuccess());
                        this.router.navigate( ['/home']);
                        
                    }
        );
     }

    select( item ) {
        this.selected.push( item );
        this.query = '';
        this.filteredList = [];
    }

    remove( item ) {
        this.selected.splice( this.selected.indexOf( item ), 1 );
    }

    handleClick( event ) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if ( clickedComponent === this.elementRef.nativeElement ) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while ( clickedComponent );
        if ( !inside ) {
            this.filteredList = [];
        }
    }
    
    isCurrentLang( lang: string ) {
        // check if the selected lang is current lang
        return lang === this._translate.currentLang;
    }

    selectLang( lang: string ) {
        // set current lang;
        this.currentLang = this.supportedLangs.find( x => x.value === lang ).display;
        this._translate.use( lang );
    }

}