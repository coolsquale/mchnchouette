import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { HomeModule } from './home/home.module';
import { UsersModule } from './users/users.module';
import { EntrevueModule } from './entrevue/entrevue.module';
import { UniteHModule } from './uniteh/uniteh.module';
import { SignUpModule } from './signup/signup.module';
import { ServicesModule } from './services/services.module';
import { LoginModule } from './login/login.module';
import { AUTH_PROVIDERS } from 'angular2-jwt';
import { AuthGuard } from './common/auth.guard';
import { AuthenticationService } from './login/authentification.service';
import { EntrevueService } from './entrevue/entrevue.service';
import { UsersService } from './users/users.service';
import { TermsAndConditionsModule } from './agreement/termsandconditions.module';
import { SharedModule } from './utils/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatTabsModule, NoConflictStyleCompatibilityMode } from '@angular/material';
import {HttpClientModule} from '@angular/common/http';

@NgModule( {
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        SharedModule.forRoot(),
        routing,HttpClientModule,
        HomeModule,
        UsersModule,
        EntrevueModule,
        UniteHModule,
        SignUpModule,
        ServicesModule,
        LoginModule,
        ReactiveFormsModule, NoopAnimationsModule, MatDialogModule, MatTabsModule, NoConflictStyleCompatibilityMode
    ],
    providers: [AuthenticationService, EntrevueService, AuthGuard,UsersService],
    bootstrap: [AppComponent]
})
export class AppModule { }
