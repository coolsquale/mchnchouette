import { Component, ViewEncapsulation, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { AuthenticationService } from '../login/authentification.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { PasswordValidation } from '../utils/validation.component';
declare var System: any;
@Component( {
    selector: 'uzo-signup',
    templateUrl: './signup.component.html'
})
export class SignUp implements OnInit {
    public error = '';
    public path = '';
    public file_srcs: string[] = [];
    public debug_size_before: string[] = [];
    public debug_size_after: string[] = [];
    public insertForm: FormGroup;
    public firstname: FormControl;
    public lastname: FormControl;
    public email: FormControl;
    public password: FormControl;
    public confirmpassword: FormControl;
    public termsAndConditions: FormControl; 
    public valid:boolean = true;
    public errorMessage:string; 
    public errorCode:number;
    constructor( public router: Router, private _fb: FormBuilder,private changeDetectorRef: ChangeDetectorRef,
        private _authenticationService: AuthenticationService ) {
    }
    ngOnInit() {
        this.firstname = new FormControl( '' ,[Validators.required] );
        this.lastname = new FormControl( '',[Validators.required]  );
        this.email = new FormControl( '', [Validators.required] );
        this.password = new FormControl( '' , [Validators.required, Validators.minLength(6)]);
        this.confirmpassword = new FormControl( '' , [Validators.required, Validators.minLength(6)]);
        this.termsAndConditions = new FormControl( '',[Validators.required]  );
        this.insertForm = this._fb.group( {
            'firstname': this.firstname,
            'lastname': this.lastname,
            'email': this.email,
            'password': this.password,
            'confirmpassword': this.confirmpassword,
            'termsAndConditions': this.termsAndConditions,
        },{validator: PasswordValidation.MatchPassword /*your validation method*/});

    }
    signup( valid:boolean ) {
       // event.preventDefault();
        this.valid = valid;
        if(!valid){
            return;
        }
        this._authenticationService.signup( this.insertForm.controls["firstname"].value,this.insertForm.controls["lastname"].value,
                            this.insertForm.controls["email"].value, this.insertForm.controls["password"].value, this.insertForm.controls["confirmpassword"].value)
                        .subscribe( result => this.router.navigate( ['/home'] ),
                                error => {
                                    this.errorMessage=error.body.message;
                                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                                    console.log(error);
                                    }
                        );
    }

    fileChange( input ) {
        this.readFiles( input.files );
    }

    readFile( file, reader, callback ) {
        reader.onload = () => {
            callback( reader.result );
            //this.model.student_img = reader.result;
            console.log( reader.result );
        }
        reader.readAsDataURL( file );
    }

    readFiles( files, index = 0 ) {
        // Create the file reader
        let reader = new FileReader();

        // If there is a file
        if ( index in files ) {
            // Start reading this file
            this.readFile( files[index], reader, ( result ) => {
                // Create an img element and add the image file data to it
                var img = document.createElement( "img" );
                img.src = result;

                // Send this img to the resize function (and wait for callback)
                this.resize( img, 250, 250, ( resized_jpeg, before, after ) => {
                    // For debugging (size in bytes before and after)
                    this.debug_size_before.push( before );
                    this.debug_size_after.push( after );

                    // Add the resized jpeg img source to a list for preview
                    // This is also the file you want to upload. (either as a
                    // base64 string or img.src = resized_jpeg if you prefer a file). 
                    this.file_srcs.push( resized_jpeg );

                    // Read the next file;
                    this.readFiles( files, index + 1 );
                });
            });
        } else {
            // When all files are done This forces a change detection
            this.changeDetectorRef.detectChanges();
        }
    }

    resize( img, MAX_WIDTH: number, MAX_HEIGHT: number, callback ) {
        // This will wait until the img is loaded before calling this function
        return img.onload = () => {

            // Get the images current width and height
            var width = img.width;
            var height = img.height;

            // Set the WxH to fit the Max values (but maintain proportions)
            if ( width > height ) {
                if ( width > MAX_WIDTH ) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            } else {
                if ( height > MAX_HEIGHT ) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }

            // create a canvas object
            var canvas = document.createElement( "canvas" );
            // Set the canvas to the new calculated dimensions
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext( "2d" );
            ctx.drawImage( img, 0, 0, width, height );
            // Get this encoded as a jpeg
            // IMPORTANT: 'jpeg' NOT 'jpg'
            var dataUrl = canvas.toDataURL( 'image/jpeg' );
            // callback with the results
            callback( dataUrl, img.src.length, dataUrl.length );
        };
    }
}

