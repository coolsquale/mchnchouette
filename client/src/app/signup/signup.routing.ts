/**
 * New typescript file
 */
import { Routes, RouterModule } from '@angular/router'
import { SignUp } from './signup.component'

const appRoutes: Routes = [
    { path: '', component: SignUp }
]

export const signupRouting = RouterModule.forChild(appRoutes);