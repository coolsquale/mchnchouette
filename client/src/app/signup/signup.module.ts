import { NgModule } from '@angular/core'
import { SignUp } from './signup.component'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { signupRouting } from './signup.routing'
import { AuthenticationService } from '../login/authentification.service';
import {SharedModule} from '../utils/shared.module';

@NgModule(
    {
        declarations: [
          SignUp
        ],
        imports: [
           SharedModule,
            CommonModule,
            FormsModule,
            signupRouting,
            ReactiveFormsModule
        ],
        exports: [SignUp],
        providers: [AuthenticationService]
    }
)
export class SignUpModule {
}