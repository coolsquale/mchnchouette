import { NgModule  } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TermsAndConditionsComponent} from './termsandconditions.component';
import { termsandconditionsRouting } from './termsandconditions.routing';
import {SharedModule} from '../utils/shared.module';
@NgModule({
  imports: [ CommonModule, termsandconditionsRouting ,SharedModule],
  declarations: [ TermsAndConditionsComponent ]
})
export class TermsAndConditionsModule {}
