import { Routes, RouterModule } from '@angular/router';

import {TermsAndConditionsComponent} from './termsandconditions.component';

const appRoutes: Routes = [
    { path: '', component: TermsAndConditionsComponent }
];

export const termsandconditionsRouting = RouterModule.forChild(appRoutes);
