export var  appConfigs = {
    "system": {
        "applicationName": "Mighty Mouse",
        "applicationUrl": "https://mchnchouette.azurewebsites.net",
        "oauthUrl": "/oauth/token",
        "tkUserUrl": "/users/names/",
        "urlEntrevue":"/entrevue",
        "urlUniteH":"/unitehemeneutique",
        "apiDefaultEndPoint":"/default/",
        "signupEndPoint":"/signup",
        "logoutEndPoint":"/logout",
        "resetEndPoint":"/resetpassword",
        "updateEndPoint":"/users/password",
        "tokenValidationEndPoint":"/registrationtionConfirm"
      },
      "year":31104000000
      
}