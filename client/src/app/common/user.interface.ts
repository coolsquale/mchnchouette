/**
 * New typescript file
 */
export interface IUser {
    id?: number;
    firstName?: string;
    lastName?: string;
    password: string;
    email: string;
}