import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';
import { AuthenticationService } from '../login/authentification.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    connected: boolean;

    constructor( private router: Router, private _authenticationService: AuthenticationService ) {
    }
    canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
        return this._authenticationService.loggedIn().map( data => {
        this.connected = data;
            if ( this.connected ) {
                // logged in so return true
                return true;
            }
            // not logged in so redirect to login page with the return url and return false
            this.router.navigate( ['/login'], { queryParams: { returnUrl: state.url } });
            return false;
        })
    }
    canActivateChild( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
        return this.canActivate( route, state );
    }
}