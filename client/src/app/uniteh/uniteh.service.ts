/**
 * New typescript file
 */
import { Injectable } from '@angular/core'
import { IUniteH } from './uniteh.interface'
import { IUniteHComment } from './unitehcomment.interface'
import { Data } from '../common/data.interface'
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import { appConfigs } from '../appConfigs'
import 'rxjs/Rx'


@Injectable()
export class UniteHService {
    private apiEndpoint: string;
    private baseUrl: string;
    private urlUniteH: string;
    private apiDefaultEndPoint: string;
    private unitehs: IUniteH[] = [];
    private uniteh: IUniteH;

    constructor( private _http: Http) {
        this.baseUrl = appConfigs.system.applicationUrl;
        this.urlUniteH = appConfigs.system.urlUniteH;
        this.apiEndpoint = this.baseUrl + this.urlUniteH;
    }

    /**
     * create trip ad
     */
    createUniteH( uniteh: IUniteH ): Observable<Data> {
        console.log(uniteh);
        return this._http.post(
            this.apiEndpoint + "/", JSON.stringify( uniteh ), this.options() )
            .map(( results: Response ) => {
                return results.json();
            })
    }
    updateUniteH( uniteh: IUniteH ): Observable<Data> {
        console.log(uniteh);
        return this._http.put(
            this.apiEndpoint + "/update", JSON.stringify( uniteh ), this.options() )
            .map(( results: Response ) => {
                return results.json();
            })
    }
    addUniteHComment( id: number, comment: IUniteHComment ): Observable<Data> {
        console.log(comment);
        return this._http.post(
            this.apiEndpoint + "/comment/" + id, JSON.stringify( comment ), this.options() )
            .map(( results: Response ) => {
                return results.json();
            })
    }
    getUniteHById( id: number ): Observable<IUniteH> {
        let h = new Headers();
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Access-Control-Allow-Origin', '*' );
        return this._http
            .get( this.apiEndpoint + "/" + id, { headers: h })
            .map(( results: Response ) => {
                this.uniteh = results.json().body;
                return this.uniteh;
            })
    }

    deleteUniteH( uniteId: number ): Observable<boolean> {
        return this._http
            .delete(
            this.apiEndpoint + "/delete/" + uniteId, this.options() )
            .map(( response: Response ) => {
                return (response.json().statusCode==1)
            });
    }

    getUnites(): Observable<IUniteH[]> {
        let h = new Headers();
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Access-Control-Allow-Origin', '*' );
        return this._http
            .get( this.apiEndpoint+"/all", { headers: h })
            .map(( results: Response ) => {
                this.unitehs = results.json().body;
                return this.unitehs;
            })
    }

    private options(): any {
        // create authorization header with jwt token
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let headers = new Headers();
            let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
            headers.append( 'Content-Type', 'application/json' );
            headers.append( 'Access-Control-Allow-Origin', '*' );
            headers.append( 'Authorization', 'Bearer ' + currentUser.access_token );
            return new RequestOptions( { headers: headers });
        }
    }
}