/**
 * New typescript file
 */
import { Component, ViewEncapsulation, OnInit } from '@angular/core'
import { UniteHService } from './'
import { Router, ActivatedRoute } from '@angular/router'
import { IUniteH } from './uniteh.interface';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms'
import { appConfigs } from '../appConfigs';
import { PACKAGE_SIZE,CURRENCIES,WEIGHT_UNITS,ERROR_CODES, HTTP_STATUS } from '../utils/constants'
declare var System: any;
@Component(
    {
        selector: 'uniteh-list',
        templateUrl: './uniteh-list.component.html',
        //    styleUrls: ['ads-list.component.css'],
        encapsulation: ViewEncapsulation.Emulated,
        styles: [`
                 .sebm-google-map-container {
                    height: 450px;
                  }
               `]
    }
)
export class UniteHListComponent implements OnInit {
    unites: IUniteH[] = [];
    public insertForm: FormGroup;
    private usrId: number;
    selectedUniteH: IUniteH;
    isLoading: boolean = false;
    isClicked: boolean;
    public userId: FormControl;
    public comment: FormControl;
    public valid: boolean = true;
    public errorMessage:string; 
    public errorCode:number;
    get EntrevueNb(): number {
        return this.unites.length;
    }

    clicked( uniteh: IUniteH ): void {
        console.log(this.selectedUniteH);
        if(!this.selectedUniteH || this.selectedUniteH.id != uniteh.id){
            this.selectedUniteH = uniteh;
        }else{
            this.selectedUniteH = null;
        }
    }

    constructor(
                private _fb: FormBuilder,
        private _uniteHService: UniteHService,
        private _router: Router,
        private _route: ActivatedRoute ) {
        _router.events.subscribe(( val ) => {
            if ( _router.url.indexOf( "?selected" ) !== -1 ) {
                console.log("testing");
                //this.submit();
            }
        });
    }

    ngOnInit() {
        let selected = this._route.snapshot.queryParams["selected"];
        this.isLoading = true;
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
        }
        this.uniteHinit();
        this.setList(selected);
    }
        initUser() {
        // initialize our address
        this.userId = new FormControl( '' );
        return this._fb.group( {
            'id': this.userId
        });
    }
    setList(selected:any){
        this._uniteHService.getUnites( ).subscribe(
                data => { this.unites = data;},
                error => {console.log(error);},
                () => {
                    this.unites.forEach(x => x.collapse = "collapsed");
                    if(selected != null){
                        this.selectedUniteH = this.unites.find(x=> x.id == selected);
                        this.unites.find(x=> x.id == selected).select = "in";
                        this.unites.find(x=> x.id == selected).collapse = "";
                    }
                    console.log(this.unites); }
                
            );
    }

    uniteHinit() {
        console.log("Getting executed ====> tripAdsinit");
        this.userId = new FormControl( '' );
        this.comment = new FormControl( '', [Validators.maxLength( 200 )] );
        this.insertForm = this._fb.group( {
            'userId': this.userId,
            'comment': this.comment,
            'user':this.initUser()
        });
        console.log("Getting executed ====> uniteHinit end");
    }
    commenter( valid:boolean ) {
        // event.preventDefault();
        console.log("I'm clicked but doing nothing");
         this.valid = valid;
         if(!valid){
             return;
         }
         let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
        }
        this.insertForm.get( "user" ).get( "id" ).setValue( this.usrId );
        this._uniteHService.addUniteHComment(this.selectedUniteH.id,this.insertForm.value).subscribe(
                data => {
                if ( data.statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR ) {
                    this.errorMessage+=data.body.message;
                    this.errorCode = data.body.code;
                    console.log(data.body);
                    console.log("Error Message = " + this.errorMessage);
                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                } else if ( data.statusCode === HTTP_STATUS.OK ) {
                    
                    //this._router.navigate( ['/uniteh?selected=' + this.selectedUniteH.id] );
                    this.insertForm.reset();
                    this.setList(this.selectedUniteH.id);
                }
            },
            error => {
                this.errorMessage = " Session Expiré, Veuillez vous connecter pour continuer !";
                this.errorCode = error.status;
                System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());console.log( error );},
            () => {}
        );
     }
    /*
    submit() {
        let selected = this._route.snapshot.queryParams["selectList"];
        this.isLoading = true;
        let sub: ISubscription = this._entrevueService
            .getEntrevues( ).finally(() => sub.unsubscribe() )
            .subscribe(
            data => this.entrevues = data,
            error => console.log( error ),
            () => { this.isLoading = false; this.more = this.entrevues.length > 5; }
            );
    }*/

}