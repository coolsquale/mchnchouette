/**
 * New typescript file
 */
import { NgModule } from '@angular/core'
import { UniteHListComponent } from './uniteh-list.component'
import { UniteHDetailComponent } from './uniteh-detail.component'
import { UniteHEditComponent } from './uniteh-edit.component'
import { OrderBy } from './orderBy.pipe'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { UniteHRouting } from './uniteh.routing'
import { UniteHService } from './uniteh.service'
import { UsersService } from '../users/users.service'
import {UniteHInsertComponent} from './uniteh-insert.component'
import {SharedModule} from '../utils/shared.module';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule(
    {
        declarations: [
            UniteHListComponent,
            UniteHInsertComponent,
            UniteHDetailComponent,
            UniteHEditComponent,
            OrderBy
        ],
        imports: [
            SharedModule,
            CommonModule,
            FormsModule,
            UniteHRouting,
            ReactiveFormsModule, ClickOutsideModule
        ],
        exports: [],
        providers: [UniteHService,UsersService]
    }
)
export class UniteHModule {
}