import { Component, OnInit } from '@angular/core'
import { IUniteH } from './uniteh.interface'
import { UsersService } from '../users/users.service'
import { UniteHService } from './'
import { AuthenticationService } from '../login/authentification.service'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms'
import { ERROR_CODES, HTTP_STATUS } from '../utils/constants'
import { IUser } from '../common/user.interface';
declare var System: any;
@Component(
    {
        templateUrl: './uniteh-edit.component.html',
        styles: [],
    }
)
export class UniteHEditComponent implements OnInit {
    
    public unite: IUniteH;
    public user: IUser;
    public id: number;
    public delete:boolean = false;
    public insertForm: FormGroup;
    private usrId: number;
    public selected: number;


    public errorMessage:string; 
    public errorCode:number;
    public userId: FormControl;
    public title: FormControl;
    public uniteId: FormControl;
    public valid: boolean = true;
    public notFound: boolean = false;

    constructor(
        private _fb: FormBuilder,
        private _uniteHService: UniteHService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _login: AuthenticationService,
        private _users: UsersService ) { }

    ngOnInit() {
        this.id = + this._route.snapshot.params["id"];
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
        }
        this._uniteHService
            .getUniteHById( this.id )
            .subscribe(
                    data => {this.unite = data; console.log(data);},
                    error => {console.log( error ); this.notFound = true;},
                    () => {
                        this.uniteHinit();
                        console.log(this.unite);
                        this._users.getUserById(this.unite.userId).subscribe(
                                data => this.user = data,
                                error => console.log( error ),
                                () => {console.log(this.user);}
                );}
            );
    }

    uniteHinit() {
        console.log("Getting executed ====> tripAdsinit");
        this.userId = new FormControl( this.usrId );
        this.title = new FormControl( this.unite.title, [Validators.required, Validators.minLength( 10 ), Validators.maxLength( 200 )] );
        this.uniteId = new FormControl(this.unite.id, [Validators.maxLength( 200 )] );
        this.insertForm = this._fb.group( {
            'userId': this.userId,
            'title': this.title,
            'id': this.uniteId
        });
        console.log("Getting executed ====> uniteHinit end");
    }
    onSubmit(valid:boolean) {
        this.valid = valid;
        if(!valid){
            return;
        }
        this.insertForm.get( "userId" ).setValue( this.usrId );
        this._uniteHService.updateUniteH( this.insertForm.value ).subscribe(
            data => {
                if ( data.statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR ) {
                    this.errorMessage+=data.body.message;
                    this.errorCode = data.body.code;
                    console.log(data.body);
                    console.log("Error Message = " + this.errorMessage);
                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                } else if ( data.statusCode === HTTP_STATUS.OK ) {
                    this._router.navigate( ['uniteh/details/' + this.unite.id] );
                }
            },
            error => console.log( error )
        );
    }
    ShowDelete() {
        this.delete = true;
    }
    Delete() {
        
        this._uniteHService.deleteUniteH( this.unite.id ).subscribe(
            data => {
                this.delete = false;
                document.getElementById("soap-popupbox").click();
                this._router.navigate( ['uniteh'] );
                
            },
            error => console.log( error )
        );
    }
    Cancel( event: any) {
        event.preventDefault();
        this.delete = false;
        document.getElementById("soap-popupbox").click();
    }
}