
export interface IUniteHComment{
    id?: number;
    userId?: number;
    comment: string;
    createdOn: Date;
}