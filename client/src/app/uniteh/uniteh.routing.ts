/**
 * New typescript file
 */
import { Routes, RouterModule } from '@angular/router'

import {UniteHListComponent} from './uniteh-list.component'
import {UniteHInsertComponent} from './uniteh-insert.component'
import {UniteHDetailComponent} from './uniteh-detail.component'
import {UniteHEditComponent} from './uniteh-edit.component'
import { AuthGuard } from '../common/auth.guard';
const appRoutes: Routes = [
    { path: 'create', component: UniteHInsertComponent, canActivate: [AuthGuard] },
    { path: 'details/:id', component: UniteHDetailComponent },
    { path: 'edit/:id', component: UniteHEditComponent , canActivate: [AuthGuard]},
     { path: '', component: UniteHListComponent}
    
]

export const UniteHRouting = RouterModule.forChild(appRoutes);