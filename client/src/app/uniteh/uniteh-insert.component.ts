/**
 * New typescript file
 */
import { Component, OnInit } from '@angular/core'
import { IUniteH } from './uniteh.interface'
import { UniteHService } from './'
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms'
import { UsersService } from '../users/users.service'
import { PasswordValidation,DateValidation} from '../utils/validation.component';
import { ERROR_CODES, HTTP_STATUS } from '../utils/constants'
declare var System: any;
@Component(
    {
        templateUrl: './uniteh-insert.component.html',
        styles: [],
    }
)
export class UniteHInsertComponent implements OnInit {

    //insertForm: FormGroup;
public insertForm: FormGroup;
private usrId: number;
public selected: number;


public errorMessage:string; 
public errorCode:number;
public userId: FormControl;
public title: FormControl;
public comment: FormControl;
public uniteh: IUniteH;
public valid: boolean = true;
public delete: boolean = true;

    constructor(
        private _fb: FormBuilder,
        private _router: Router,
        private _uniteHService: UniteHService,
        private _usersService: UsersService ) {
    }

    onSubmit(valid:boolean) {
        this.valid = valid;
        if(!valid){
            return;
        }
        this.insertForm.get( "userId" ).setValue( this.usrId );
        this.insertForm.get( "user" ).get( "id" ).setValue( this.usrId );
        let comment = this.insertForm.get( "title" ).value;
        this._uniteHService.createUniteH( this.insertForm.value ).subscribe(
            data => {
                if ( data.statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR ) {
                    this.errorMessage+=data.body.message;
                    this.errorCode = data.body.code;
                    console.log(data.body);
                    console.log("Error Message = " + this.errorMessage);
                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                } else if ( data.statusCode === HTTP_STATUS.OK ) {
                    this.uniteh = data.body;
                    if(this.insertForm.get( "comment" ).value != ""){
                        this._uniteHService.addUniteHComment(this.uniteh.id,this.insertForm.value).subscribe(
                             data => {
                                if ( data.statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR ) {
                                    this.errorMessage+=data.body.message;
                                    this.errorCode = data.body.code;
                                    console.log(data.body);
                                    console.log("Error Message = " + this.errorMessage);
                                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                                } else if ( data.statusCode === HTTP_STATUS.OK ) {
                                    
                                    this._router.navigate( ['uniteh/details/' + this.uniteh.id] );
                                }
                            },
                            error => console.log( error ),
                            () => {}
                        );
                    }else{
                        this._router.navigate( ['uniteh/details/' + this.uniteh.id] );
                    }
                    
                }
            },
            error => console.log( error ));
    }
  
    ngOnInit() {
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
            this.uniteHinit();
        } else {
            this._router.navigate( ['/login'], { queryParams: { returnUrl: '/uniteh/create' ,'expired':'true'} });
        }

    }

    initUser() {
        // initialize our address
        this.userId = new FormControl( '' );
        return this._fb.group( {
            'id': this.userId
        });
    }


    uniteHinit() {
        console.log("Getting executed ====> tripAdsinit");
        this.userId = new FormControl( '' );
        this.title = new FormControl( '', [Validators.required, Validators.minLength( 10 ), Validators.maxLength( 200 )] );
        this.comment = new FormControl( '', [Validators.maxLength( 200 )] );
        this.insertForm = this._fb.group( {
            'userId': this.userId,
            'title': this.title,
            'comment': this.comment,
            'user':this.initUser()
        });
        console.log("Getting executed ====> uniteHinit end");
    }


}