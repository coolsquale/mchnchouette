import { IUniteHComment } from './unitehcomment.interface'
import { IUser } from '../common/user.interface'
export interface IUniteH {
    id?:number;
    title:string;
    userId?:number
    createdOn: Date;
    comments:IUniteHComment[];
    user:IUser;
    select:string;
    collapse:string;
}