import { Component, OnInit } from '@angular/core'
import { IUniteH } from './uniteh.interface'
import { UsersService } from '../users/users.service'
import { UniteHService } from './'
import { AuthenticationService } from '../login/authentification.service'
import { Router, ActivatedRoute } from '@angular/router'
import { IUser } from '../common/user.interface';
@Component(
    {
        templateUrl: './uniteh-detail.component.html',
        styles: [`
                 .det {
                    color: #01b7f2;
                    background: #fff;
                    display: block;
                    padding: 0px 20px;
                    font-size: 1.5em;
                    font-weight: bold;
                    height: 40px;
                    line-height: 40px;
                    text-decoration: none;
                    text-transform: uppercase;
                    white-space: nowrap;
                    letter-spacing: 0.04em;
                    outline: 0;
                    box-sizing: border-box;
                    margin: 0;
                    -webkit-tap-highlight-color: transparent;
                    zoom: 1;
                    cursor: auto;
                    -webkit-font-smoothing: antialiased;
                  }
                .blur{
                z-index:1;
                top: 0;
                    right: 0;
                    width: 100%;
                    height: 100%;
                    display: block;
                    position: absolute;
                    padding-top: 10%;
                    margin-left: -305px;
                    align-content: center;
                    background: rgba(34,34,34,0.75);
                    overflow: hidden;
                }
                .formbox{
                    width:50%;
                    margin:auto; 
                    top:100px;
                }
                .sebm-google-map-container{
                     height: 300px;
                 }
               `],
    }
)
export class UniteHDetailComponent implements OnInit {
    
    unite: IUniteH;
    user: IUser;
    id: number;
    connected:boolean = false;

    constructor(
        private _uniteHService: UniteHService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _login: AuthenticationService,
        private _users: UsersService ) { }

    ngOnInit() {
        this.id = + this._route.snapshot.params["id"];
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        let userId: number = 0;
        if ( currentUser && currentUser.access_token ) {
            userId = currentUser.userId;
        }
        this._uniteHService
            .getUniteHById( this.id )
            .subscribe(
                    data => {this.unite = data;},
                    error => console.log( error ),
                    () => {
                        console.log(this.unite);
                        this._users.getUserById(this.unite.userId).subscribe(
                                data => this.user = data,
                                error => console.log( error ),
                                () => {console.log(this.user);}
                );}
            );
    }

}