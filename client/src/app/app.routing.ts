import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from './common/auth.guard';


const appRoutes: Routes = [
   { path: '', loadChildren: 'app/home/home.module#HomeModule'},
   { path: 'home',  loadChildren: 'app/home/home.module#HomeModule' },
   { path: 'entrevue', loadChildren: 'app/entrevue/entrevue.module#EntrevueModule'},
   { path: 'uniteh', loadChildren: 'app/uniteh/uniteh.module#UniteHModule'},
   { path: 'users', loadChildren: 'app/users/users.module#UsersModule', canActivate: [AuthGuard] },
   { path: 'login', loadChildren: 'app/login/login.module#LoginModule' },
   { path: 'signup', loadChildren: 'app/signup/signup.module#SignUpModule' },
   { path: 'services', loadChildren: 'app/services/services.module#ServicesModule' },
   { path: 'agreement', loadChildren: 'app/agreement/termsandconditions.module#TermsAndConditionsModule' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

