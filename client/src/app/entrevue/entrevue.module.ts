/**
 * New typescript file
 */
import { NgModule } from '@angular/core'
import { EntrevueListComponent } from './entrevue-list.component'
import { EntrevueDetailComponent } from './entrevue-detail.component'
import { OrderBy } from './orderBy.pipe'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { EntrevueRouting } from './entrevue.routing'
import { EntrevueService } from './entrevue.service'
import { UsersService } from '../users/users.service'
import {EntrevueInsertComponent} from './entrevue-insert.component'
import {SharedModule} from '../utils/shared.module';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule(
    {
        declarations: [
            EntrevueListComponent,
            EntrevueInsertComponent,
            EntrevueDetailComponent,
            OrderBy
        ],
        imports: [
            SharedModule,
            CommonModule,
            FormsModule,
            EntrevueRouting,
            ReactiveFormsModule, ClickOutsideModule
        ],
        exports: [],
        providers: [EntrevueService,UsersService]
    }
)
export class EntrevueModule {
}