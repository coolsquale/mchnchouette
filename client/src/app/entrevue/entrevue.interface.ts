/**
 * New typescript file
 */
import { IParagraph } from './paragraph.interface'
export interface IEntrevue{
    id?: number;
    userId?: number;
    entrevue: string;
    title: string;
    createdOn: Date;
    paragraphs:IParagraph[];
}