/**
 * New typescript file
 */

export interface IParagraph {
    paragraphId?:number;
    paragraph:string;
    createdOn: Date;
}