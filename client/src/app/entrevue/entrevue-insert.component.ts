/**
 * New typescript file
 */
import { Component, OnInit } from '@angular/core'
import { IEntrevue } from './entrevue.interface'
import { EntrevueService } from './'
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms'
import { UsersService } from '../users/users.service'
import { PasswordValidation,DateValidation} from '../utils/validation.component';
import { PACKAGE_SIZE,CURRENCIES,WEIGHT_UNITS,ERROR_CODES, HTTP_STATUS } from '../utils/constants'
declare var System: any;
@Component(
    {
        templateUrl: './entrevue-insert.component.html',
        styles: [],
    }
)
export class EntrevueInsertComponent implements OnInit {

    //insertForm: FormGroup;
public insertForm: FormGroup;
private usrId: number;
public selected: number;


public errorMessage:string; 
public errorCode:number;
public userId: FormControl;
public title: FormControl;
public content: FormControl;
public entrevue: IEntrevue;
public valid: boolean = true;

    constructor(
        private _fb: FormBuilder,
        private _router: Router,
        private _entrevueService: EntrevueService,
        private _usersService: UsersService ) {
    }

    onSubmit(valid:boolean) {
        this.valid = valid;
        if(!valid){
            return;
        }
        this.insertForm.get( "userId" ).setValue( this.usrId );
        this._entrevueService.createEntrevue( this.insertForm.value ).subscribe(
            data => {
                if ( data.statusCode === HTTP_STATUS.INTERNAL_SERVER_ERROR ) {
                    this.errorMessage+=data.body.message;
                    this.errorCode = data.body.code;
                    console.log(data.body);
                    console.log("Error Message = " + this.errorMessage);
                    System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                } else if ( data.statusCode === HTTP_STATUS.OK ) {
                    this.entrevue = data.body;
                }
            },
            error => console.log( error ),
            () => {
                this._router.navigate( ['entrevue/details/' + this.entrevue.id] );
            });
    }
  
    ngOnInit() {
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            this.usrId = currentUser.userId;
            this.entrevueinit();
        } else {
            this._router.navigate( ['/login'], { queryParams: { connected:false,returnUrls: '/entrevue/create' } });
        }

    }

    initUser() {
        // initialize our address
        this.userId = new FormControl( '' );
        return this._fb.group( {
            'id': this.userId
        });
    }


    entrevueinit() {
        console.log("Getting executed ====> entrevueinit");
        this.userId = new FormControl( '' );
        this.title = new FormControl( '', [Validators.required, Validators.minLength( 10 ), Validators.maxLength( 200 )] );
        this.content = new FormControl( '', [Validators.required, Validators.minLength( 20 )] );
        this.insertForm = this._fb.group( {
            'userId': this.userId,
            'title': this.title,
            'entrevue': this.content,
        });
        console.log("Getting executed ====> entrevueinit end");
    }


}