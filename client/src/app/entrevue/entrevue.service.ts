/**
 * New typescript file
 */
import { Injectable } from '@angular/core'
import { IEntrevue } from './entrevue.interface'
import { IParagraph } from './paragraph.interface'
import { Data } from '../common/data.interface'
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import { appConfigs } from '../appConfigs'
import 'rxjs/Rx'


@Injectable()
export class EntrevueService {
    private apiEndpoint: string;
    private baseUrl: string;
    private urlEntrevue: string;
    private apiDefaultEndPoint: string;
    private entrevues: IEntrevue[] = [];
    private entrevue: IEntrevue;

    constructor( private _http: Http) {
        this.baseUrl = appConfigs.system.applicationUrl;
        this.urlEntrevue = appConfigs.system.urlEntrevue;
        this.apiEndpoint = this.baseUrl + this.urlEntrevue;
    }

    /**
     * create trip ad
     */
    createEntrevue( entrevue: IEntrevue ): Observable<Data> {
        console.log(entrevue);
        return this._http.post(
            this.apiEndpoint + "/", JSON.stringify( entrevue ), this.options() )
            .map(( results: Response ) => {
                return results.json();
            })
    }
    getEntrevueById( id: number ): Observable<IEntrevue> {
        let h = new Headers();
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Access-Control-Allow-Origin', '*' );
        return this._http
            .get( this.apiEndpoint + "/" + id, { headers: h })
            .map(( results: Response ) => {
                this.entrevue = results.json().body;
                return this.entrevue;
            })
    }

    deleteEntrevue( entrevueId: number ): Observable<boolean> {
        return this._http
            .delete(
            this.apiEndpoint + "/delete/" + entrevueId, this.options() )
            .map(( response: Response ) => {
                return (response.json().statusCode==1)
            });
    }

    getEntrevues(): Observable<IEntrevue[]> {
        let h = new Headers();
        h.append( 'Content-Type', 'application/json' );
        h.append( 'Access-Control-Allow-Origin', '*' );
        return this._http
            .get( this.apiEndpoint+"/all", { headers: h })
            .map(( results: Response ) => {
                this.entrevues = results.json().body;
                return this.entrevues;
            })
    }

    private options(): any {
        // create authorization header with jwt token
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let headers = new Headers();
            let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
            headers.append( 'Content-Type', 'application/json' );
            headers.append( 'Access-Control-Allow-Origin', '*' );
            headers.append( 'Authorization', 'Bearer ' + currentUser.access_token );
            return new RequestOptions( { headers: headers });
        }
    }
    /*
    function toParagraph( r: any ): IParagraph {
        let address = <IParagraph>( {
            addressId: r.addressId,
            addressType: r.addressType,
            unit: r.unit,
            location: toLocation( r.location )
        });
        return address;
    }

    function toEntrevue( r: any ): IEntrevue {

        let entrevue = <IEntrevue>( {
                id: r.id,
        userId: r.userId,
        entrevue: r.userId,
        title: r.userId,
        createdOn: r.userId,
        paragraphs:toParagraphe( r.paragraphs )
        });
        return entrevue;
    }*/

}