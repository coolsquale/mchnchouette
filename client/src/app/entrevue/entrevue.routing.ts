/**
 * New typescript file
 */
import { Routes, RouterModule } from '@angular/router'

import {EntrevueListComponent} from './entrevue-list.component'
import {EntrevueInsertComponent} from './entrevue-insert.component'
import {EntrevueDetailComponent} from './entrevue-detail.component'
import { AuthGuard } from '../common/auth.guard';
const appRoutes: Routes = [
    { path: 'create', component: EntrevueInsertComponent, canActivate: [AuthGuard] },
    { path: 'details/:id', component: EntrevueDetailComponent },
     { path: '', component: EntrevueListComponent}
    
]

export const EntrevueRouting = RouterModule.forChild(appRoutes);