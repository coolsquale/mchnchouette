/**
 * New typescript file
 */
import { Component, ViewEncapsulation, OnInit } from '@angular/core'
import { EntrevueService } from './'
import { Router, ActivatedRoute } from '@angular/router'
import { IEntrevue } from './entrevue.interface';
//import { ISubscription } from "rxjs/Subscription";
import { appConfigs } from '../appConfigs';
declare var System: any;

@Component(
    {
        selector: 'entrevue-list',
        templateUrl: './entrevue-list.component.html',
        //    styleUrls: ['ads-list.component.css'],
        encapsulation: ViewEncapsulation.Emulated,
        styles: [`
                 .sebm-google-map-container {
                    height: 450px;
                  }
               `]
    }
)
export class EntrevueListComponent implements OnInit {
    title: string = "Products";
    entrevues: IEntrevue[] = [];
    selectedEntrevue: IEntrevue;
    adsSort: Array<Array<IEntrevue>> = new Array<Array<IEntrevue>>();
    isLoading: boolean = false;
    sorter: string = "-price";
    asc: boolean = true;
    next: number = 5;
    previous: number = 0;
    more: boolean = true;
    mapShow: boolean = false;
    zoom: number = 10;
    max: number = 250;
    public error: boolean = false;
    public delete:boolean = false;
    public errorMessage:string; 
    public errorCode:number;
    get EntrevueNb(): number {
        return this.entrevues.length;
    }

    onSelect( entrevue: IEntrevue ): void {
        this.selectedEntrevue = entrevue;
        //this._router.navigateByUrl( "/entrevue/" + entrevue.id );
    }

    constructor(
        private _entrevueService: EntrevueService,
        private _router: Router,
        private _route: ActivatedRoute ) {
        _router.events.subscribe(( val ) => {
            if ( _router.url.indexOf( "?selectList" ) !== -1 ) {
                //this.submit();
            }
        });
    }

    ngOnInit() {
        let selected = this._route.snapshot.queryParams["selectList"];
        this.isLoading = true;
        /*
        let sub: ISubscription = this._entrevueService
            .getEntrevues().finally(() => sub.unsubscribe() )
            .subscribe(
            data => this.entrevues = data,
            error => console.log( error ),
            () => { this.isLoading = false; this.more = this.entrevues.length > 5; console.log( this.entrevues ); }
            );*/
            this.initList();
            

    }
    initList(){
        this._entrevueService.getEntrevues( ).subscribe(
            data => { this.entrevues = data;},
            error => {console.log(error);},
            () => {console.log(this.entrevues); }
            
        );
    }
    /*
    submit() {
        let selected = this._route.snapshot.queryParams["selectList"];
        this.isLoading = true;
        let sub: ISubscription = this._entrevueService
            .getEntrevues( ).finally(() => sub.unsubscribe() )
            .subscribe(
            data => this.entrevues = data,
            error => console.log( error ),
            () => { this.isLoading = false; this.more = this.entrevues.length > 5; }
            );
    }*/
    ShowDelete() {
        this.delete = true;
        this.error=false;
    }
    Delete() {
        this._entrevueService.deleteEntrevue( this.selectedEntrevue.id ).subscribe(
            data => {
                
                    this.initList();
                
            },
            error => {
                this.errorMessage = " Session Expiré, Veuillez vous connecter pour continuer !";
                this.errorCode = error.status;
                this.error=true;
                System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());console.log( error );},
            () => {});
    }
    Cancel( event: any) {
        event.preventDefault();
        this.delete = false;
        document.getElementById("soap-popupbox").click();
    }
    clicked( uniteh: IEntrevue ): void {
        console.log(this.selectedEntrevue);
        if(!this.selectedEntrevue || this.selectedEntrevue.id != uniteh.id){
            this.selectedEntrevue = uniteh;
        }else{
            this.selectedEntrevue = null;
        }
    }

}