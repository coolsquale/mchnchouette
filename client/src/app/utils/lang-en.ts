export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
    // app translation 
   'Home': 'Home',
   'home-title': 'MachinChouette - Home',
   'home-title-alt': 'MachinChouette web app',
   'entrevue': 'Interview',
   'entrevue-list':'Interviews List',
   'entrevue-create': 'Add Interview',
   'entrevue-info':'about Interview',
   'entrevue-title-holder':'content title',
   'entrevue-content-holder':'content text',
   'button-delete':'Delete selection',
   'added-by':'Added by',
   'created-on':'Created On',
   'paragraphs':'Paragraph(s)',
   'added-on':'Added on',
   'button-create':'Add',
   'button-cancel':'Cancel',
   'button-update':'Update',
   'button-comment':'Comment',
   'uniteh': 'Hemeneutique unit',
   'uniteh-create': 'Add Hemeneutique unit',
   'uniteh-list': 'Hemeneutiques unit List',
   'uniteh-edit':'Edit',
   'uniteh-details':'Hemeneutique unit details',
   'comment-holder':'Add a comment',
   'comment':'Comment',
   'available-action':'Available Actions',
   'warning-title':'Warning',
   'warning-message':'Note that deleting this content is permanent and irreversible. Any data associated to this will also be deleted',
   'you-need-to-connect':'Authentication required to complete this action',
   'by':'by',
   'results-found':'Result(s) found',
   'title':'Title',
   'login-login-account':'Please login to your account.',
   'home-about-policies':'Policies',
   'home-english': 'English',
   'home-french': 'Franï¿½ais',
   'home-account': 'My Account',
   'home-logout': 'Logout',
   'home-signup': 'Signup',
   'home-login': 'Login',
   'home-required':'Required.',
   'firstname-required':'First name is required',
   'lastname-required':'Last name is required',
   'home-email-required':'Email is required',
   'home-email-valid':'Email is invalid',
   'home-password-required':'Password required',
   'home-password-confirmation':'Confirm password',
   'home-password-valid':'Password must be at least 6 chars',
   'home-password-valid-match':'Does not match password',
   'home-agreement':'I agree to the ',
   'home-terms-conditions':'Terms and conditions',
   'home-terms-conditions-valid':'You need to accept terms and conditions',
   'home-button-signup':'SIGN UP',
   'home-signup-already':'Already a Carry4U member ?',
   'home-username-required':'Username is required.',
   'home-credentials-valid':'Incorrect username/password',
   'home-password-forgot':'Forgot password ?',
   'home-remember-me':'Remember me',
   'home-button-login': 'Login',
   'home-have-account':'Don\'t have an account ?',
   'home-signup-successful':'You successfully signed up, please check your email to complete the process',
   'home-connected':'Restez connectes',
   'home-phone':'1-888-8888888',
   'home-email':'uzolab@gmail.com',
   'home-trademark':'2018 MachinChouette',
   'home-plholder-firstname':'prenom',
   'home-plholder-lastname':'nom de famille',
   'home-plholder-password':'mot de passe',
   'home-mobile-menu':'Mobile Menu Toggle',

   'ERROR':"ERREUR CODE",
   //translation for all home components goes here 
   'Recent Ads':'Annonce(s) Recente(s)',
   //translation for all adlist components goes here
   'AD Details':'Details de l\'annonce',
   //translation for all login components goes here
   'welcome-back':'Bienvenue',
   //translation for all signup component goes here
   'Subscribe':'Enregistrez-vous',
   
   // USER DASHBOARDS TRANSLATIONS
   'user-dashboard':'Dashboard',
   'user-profile':'Profile',
   'user-requests':'My requests',
   'user-trips':'My trips',
   'user-packages': 'My packages',
   'user-settings': 'Settings',
   'user-alerts': 'Alerts',
   'user-notifications': 'My Notifications',
   'user-dashboard-hi':'Hi',
   'user-dashboard-hi2':'Welcome to carry4u',
   'user-dashboard-welcome': 'All your trips and ads created with us will appear here and you\'ll be able to manage everything!',
   'user-trips-count': 'Trips to perform',
   'user-packages-count': 'Packages to perform',
   'user-trips-view': 'View my trips',
   'user-packages-view': 'View my packages',
   'user-dashboard-message1': 'This is your Dashboard, the place to check your Profile, respond to Reservation Requests, view upcoming Trip Information, and much more.',
   'user-dashboard-hint1': 'Learn How It Works',
   'user-dashboard-hint1-text': 'ï¿½ Watch a short video that shows you how Travelo works.',
   'user-dashboard-hint2': 'Get Help',
   'user-dashboard-hint2-text': 'ï¿½ View our help section and FAQs to get started on Travelo.',
   'user-dashboard-benefits': 'Benefits of carry4u Account',
   'user-dashboard-faster': 'Faster bookings with lesser clicks',
   'user-dashboard-track': 'Track travel history &amp; manage bookings',
   'user-dashboard-manage': 'Manage profile, personalize experience',
   'user-dashboard-receive': 'Receive alerts & recommendations',
   'user-dashboard-previous': 'Your Previous Bookings',
   'user-dashboard-help': 'Need carry4u Help?',
   'user-dashboard-help-text' : 'We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.',
   
      
      
   // USER PROFILE TRANSLATIONS
   'user-profile-edit': 'EDIT PROFILE',
   'user-picture-edit': 'Add/Modify',
   'user-profile-mail': 'Email',
   'user-profile-first': 'First name',
   'user-profile-last': 'Last name',



   // USERS SETTINGS
   'users-settings-account':'Account Settings',
   'users-settings-change':'Change Your Password',
   'users-settings-old':'Old Password',
   'users-settings-new':'Enter New Password',
   'users-settings-required':'Required.',
   'users-settings-pwd-required':'Password is required',
   'users-settings-pwd-length':'Password must be at least 6 chars',
   'users-settings-confirm':'Confirm New Password',
   'users-settings-pwd-match':'Does not match password',
   'users-settings-pwd-btn':'UPDATE PASSWORD',


          
   // TERMS AND CONDITIONS
   'terms-and-conditions':'Terms and conditions',

   // RESET
   'reset-password-text':'Password Reset',
   'reset-plcholder-email':'enter your email or username',
   'reset-button':'Reset'

};