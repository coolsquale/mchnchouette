/**
 * Constants used all over the app
 */
export const AD_TYPE = {
    'TRIP': 'TRIP',
    'PACKAGE': 'PACKAGE',
    'ALL': 'ALL'
};
export const REQUEST_TYPE = {
    'TRIP': 'TRIP',
    'PACKAGE': 'PACKAGE',
    'ALL': 'ALL'
};
export const SEARCH_TYPE = {
    'TRIP': 'TRIP',
    'PACKAGE': 'PACKAGE',
    'ALL': 'ALL'
};
export const REQUEST_STATUS = {
    'ACCEPTED': 'ACCEPTED',
    'UNDER_REVIEW': 'UNDER_REVIEW',
    'PENDING': 'UNDER_REVIEW',
    'REJECTED': 'REJECTED'
};
export const AD_STATUS = {
    'ACCEPTED': 'ACCEPTED',
    'UNDER_REVIEW': 'UNDER_REVIEW',
    'PENDING': 'UNDER_REVIEW',
    'REJECTED': 'REJECTED'
};
export const PACKAGE_SIZE = ['SMALL', 'MEDIUM', 'LARGE'];

export const CURRENCIES = ['CAD', 'USD', 'EUR', 'CFA'];
export const WEIGHT_UNITS = ['KG', 'LB'];

export const ACTIVITY_TYPE = {
    'LOCATION_PUBLISHED_AD': 'LOCATION_PUBLISHED_AD',
    'SEARCH_RESULT_PUBLISHED_AD': 'SEARCH_RESULT_PUBLISHED_AD',
    'PATH_PUBLISHED_TRIP_AD': 'PATH_PUBLISHED_TRIP_AD',
    'PATH_PUBLISHED_PACKAGE_AD': 'PATH_PUBLISHED_PACKAGE_AD',
    'USER_PUBLISHED_AD': 'USER_PUBLISHED_AD'
};
export const REVIEW_RATING = {
    'EXCELLENT': 'Excellent',
    'GOOD': 'Good',
    'OK': 'Ok',
    'BAD': 'Bad',
    'TERRIBLE': 'Terrible'
};
export const TRANSACTION_STATUS = {
    'OPEN': 'OPEN',
    'REJECTED': 'REJECTED',
    'ACCEPTED': 'ACCEPTED',
    'CANCELLED': 'CANCELLED',
    'EXPIRED': 'EXPIRED',
    'PARTIALLY_COMPLETED': 'PARTIALLY_COMPLETED',
    'INCOMPLETE': 'INCOMPLETE',
    'COMPLETED': 'COMPLETED',
    'DISPUTED': 'DISPUTED'
};
export const TRANSACTION_STATUS_TEXT = {
    'OPEN': 'OPEN',
    'REJECTED': 'REJECTED',
    'ACCEPTED': 'ACCEPTED',
    'CANCELLED': 'CANCELLED',
    'EXPIRED': 'EXPIRED',
    'PARTIALLY_COMPLETED': 'PARTIALLY_COMPLETED',
    'INCOMPLETE': 'INCOMPLETE',
    'COMPLETED': 'COMPLETED',
    'DISPUTED': 'DISPUTED'
};

export const USER_STATUS = {
    'ACTIVE': 'ACTIVE',
    'UNDER_REVIEW': 'UNDER_REVIEW',
    'INACTIVE': 'INACTIVE'
};
export const HTTP_STATUS = {
        'INTERNAL_SERVER_ERROR': 'INTERNAL_SERVER_ERROR',
        'OK': 'OK',
        'FOUND': 'FOUND',
        'NOT_FOUND': 'NOT_FOUND'
};
export const ERROR_CODES = {
        '1': 'Trip is nto available',
        '2': 'An unknown error occured'
};