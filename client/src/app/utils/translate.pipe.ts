import { Pipe } from '@angular/core';
import { TranslateService } from './translate.service'; // our translate service

@Pipe( {
    name: 'translate',
    pure: false
})

export class TranslatePipe {

    constructor( private _translate: TranslateService ) { }
    transform( value: string, args: any[] ): any {
        if ( !value ) return;
        return this._translate.instant( value );
    }
}