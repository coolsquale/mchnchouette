import { NgModule, ModuleWithProviders } from '@angular/core';
import { TruncatePipe } from './truncate';
import { TRANSLATION_PROVIDERS, TranslatePipe, TranslateService, PasswordValidation, EmailValidation } from './';
@NgModule( {
    declarations: [TruncatePipe, TranslatePipe],
    exports: [TruncatePipe, TranslatePipe],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [TRANSLATION_PROVIDERS, TranslateService, PasswordValidation, EmailValidation],
        };
    }
}