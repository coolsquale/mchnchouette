import {AbstractControl} from '@angular/forms';
import { DatePipe } from '@angular/common';
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value; // to get value in input tag
       let confirmPassword = AC.get('confirmpassword').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirmpassword').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }
}

export class EmailValidation {

    static MatchEmail(AC: AbstractControl) {
       let email = AC.get('email').value; // to get value in input tag
       let emailVerif = AC.get('emailVerif').value; // to get value in input tag
        if(email != emailVerif) {
            AC.get('emailVerif').setErrors( {MatchEmail: true} )
        } else {
            return null 
        }
    }
}

export class DateValidationFromString {
    
    static ValidInterval(AC: AbstractControl) {
        let datePipe = new DatePipe("en-US");
       let departure = datePipe.transform( Date.parse( AC.get('departure').value ), 'yyyy-MM-dd' ); // to get value in input tag
       let arrival = datePipe.transform( Date.parse(AC.get('arrival').value), 'yyyy-MM-dd' ); // to get value in input tag
        if(departure > arrival) {
            console.log('false');
            AC.get('arrival').setErrors( {ValidInterval: true} )
        } else {
            return null
        }
    }
}
export class DateValidation {
    
    static ValidInterval(AC: AbstractControl) {
       let departure = AC.get('departureTime').value ; // to get value in input tag
       let arrival = AC.get('arrivalTime').value; // to get value in input tag
        if(departure > arrival) {
            AC.get('arrivalTime').setErrors( {ValidInterval: true} )
        } else {
            return null
        }
    }
}

export class WeightValidation {
    static ValidWeight(AC: AbstractControl){
        let weight = AC.get('maxWeight').value ; // to get value in input tag
        if((weight <= 0)) {
            AC.get('maxWeight').setErrors( {ValidWeight: true} )
        } else {
            return null
        }
     
  }
}
export class PriceValidation {
    static ValidPrice(AC: AbstractControl){
        let price = AC.get('price').value ; // to get value in input tag
        if((price < 0)) {
            AC.get('price').setErrors( {ValidPrice: true} )
        } else {
            return null
        }
     
  }
}