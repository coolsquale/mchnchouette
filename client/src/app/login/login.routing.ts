/**
 * New typescript file
 */
import { Routes, RouterModule } from '@angular/router'
import { Login } from './login.component'
import { ResetPassword } from './resetpassword.component'
import { ActivateAccount } from './activateaccount.component'

const appRoutes: Routes = [
    { path: '', component: Login },
    { path: 'resetpassword', component: ResetPassword },
    { path: 'activate', component: ActivateAccount }
]

export const loginRouting = RouterModule.forChild( appRoutes );