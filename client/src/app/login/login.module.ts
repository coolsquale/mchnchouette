import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Login } from './login.component';
import { ResetPassword } from './resetpassword.component';
import { ActivateAccount } from './activateaccount.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { loginRouting } from './login.routing';
import { AuthenticationService } from './authentification.service';
import { SharedModule } from '../utils/shared.module';
@NgModule(
    {
        declarations: [Login, ResetPassword, ActivateAccount],
        imports: [CommonModule, FormsModule, SharedModule, loginRouting, ReactiveFormsModule],
        exports: [Login, ResetPassword, ActivateAccount],
        providers: [AuthenticationService]
    }
)
export class LoginModule {
}