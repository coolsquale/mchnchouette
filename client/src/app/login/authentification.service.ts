import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { myConfig } from '../common/auth.config';
import 'rxjs/add/operator/map'
import { contentHeaders } from '../common/headers';
import { USER_STATUS } from '../utils/constants'
import { appConfigs } from '../appConfigs';

@Injectable()
export class AuthenticationService {
    //@Output() loginEvent = new EventEmitter<boolean>();
    private tokenEndPoint: string;
    private userEndPoint: string;
    private signupEndPoint: string;
    private logoutEndPoint: string;
    private resetEndPoint: string;
    private tokenValidationEndPoint: string;
    private updateEndPoint: string;
    public access_token: string;
    public refresh_token: string;
    public userId: string;
    private baseUrl: string;
    private urlAds: string;
    private tokenuUrl: string;
    private userUrl: string;
    private logged: boolean;

    constructor( private _http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        this.access_token = currentUser && currentUser.access_token;
        this.refresh_token = currentUser && currentUser.refresh_token;
        this.baseUrl = appConfigs.system.applicationUrl;
        this.tokenuUrl = appConfigs.system.oauthUrl;
        this.userUrl = appConfigs.system.tkUserUrl;
        this.tokenEndPoint = this.baseUrl + this.tokenuUrl;
        this.userEndPoint = this.baseUrl + this.userUrl;
        this.signupEndPoint = this.baseUrl + appConfigs.system.signupEndPoint;
        this.logoutEndPoint = this.baseUrl + appConfigs.system.logoutEndPoint;
        this.resetEndPoint = this.baseUrl + appConfigs.system.resetEndPoint;
        this.updateEndPoint = this.baseUrl + appConfigs.system.updateEndPoint;
        this.tokenValidationEndPoint = this.baseUrl + appConfigs.system.tokenValidationEndPoint;
    }
    
    getLoginEvent() {
        // return this.loginEvent;
    }
    
    loggedIn(): Observable<boolean> {
        return this.tokenNotExpired();
    }
    
    login( username: string, password: string ): Observable<boolean> {
        let params = new URLSearchParams();
        params.append( 'username', username );
        params.append( 'password', password );
        params.append( 'grant_type', 'password' );
        params.append( 'scope', 'read write' );
        params.append( 'client_secret', myConfig.secret );
        params.append( 'client_id', myConfig.clientID );
        let headers = new Headers( {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json",
            "Authorization": "Basic " + btoa( myConfig.clientID + ':' + myConfig.secret )
        });

        return this._http.post( `${this.tokenEndPoint}`, params.toString(), { headers: headers })
            .map(( response: Response ) => {
                // login successful if there's a jwt token in the response
                let token = response.json().access_token;
                let refresh = response.json().refresh_token;
                if ( token ) {
                    // set token property
                    this.access_token = token;
                    this.refresh_token = refresh;
                    sessionStorage.setItem( 'access_token', token );
                    this.logged = true;
                    return this.logged;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }
    
    /**
     * Login with remember me
     */
    loginWithRememberMe( username: string, password: string, rememberMe: boolean ): Observable<boolean> {
        let params = new URLSearchParams();
        params.append( 'username', username );
        params.append( 'password', password );
        params.append( 'grant_type', 'password' );
        params.append( 'scope', 'read write' );
        params.append( 'client_secret', myConfig.secret );
        params.append( 'client_id', myConfig.clientID );
        let headers = new Headers( {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json",
            "Authorization": "Basic " + btoa( myConfig.clientID + ':' + myConfig.secret )
        });

        return this._http.post( `${this.tokenEndPoint}`, params.toString(), { headers: headers })
            .map(( response: Response ) => {
                // login successful if there's a jwt token in the response
                let token = response.json().access_token;
                let refresh = response.json().refresh_token;
                if ( token ) {
                    // set token property
                    this.access_token = token;
                    this.refresh_token = refresh;
                    sessionStorage.setItem( 'access_token', token );
                    if (rememberMe) {
                        localStorage.setItem( 'username', username );
                        localStorage.setItem( 'password', password );
                    }
                    this.logged = true;
                    return this.logged;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    /**
     * Sign user
     */
    signup( firstname: string, lastname: string, email: string, password: string, confirmpassword: string): Observable<boolean> {
        let body = { 'firstname': firstname, 'lastname': lastname, 'email': email, 'password': password, 'confirmpassword': confirmpassword }
        return this._http.post( `${this.signupEndPoint}`, body )
            .map(( response: Response ) => {
                return true;
            });
    }

    /**
     * Reset current user password
     */
    resetpassword( username: string ): Observable<boolean> {
        let params = new URLSearchParams();
        params.append( 'email', username );
        let options = new RequestOptions( { search: params });
        return this._http.post( `${this.resetEndPoint}`, {}, options )
            .map(( response: Response ) => {
                return true;
            });
    }
    
    /**
     * Update current user password
     */
    updatepassword( userId: number, password:string, newPassword:string ): Observable<boolean> {
        let body = { 'userId': userId, 'password': password, 'newPassword': newPassword}
        return this._http.put( `${this.updateEndPoint}/${userId}`, body, this.options() )
            .map(( response: Response ) => {
                return true;
            });
    }
    
    /**
     * Confirm registration using token provided
     */
    validateToken(token:string): Observable<boolean> {
        let params = new URLSearchParams();
        params.append( 'token', token );
        let options = new RequestOptions( { search: params });
        return this._http.post( this.tokenValidationEndPoint, {}, options )
            .map(( response: Response ) => {
                return true;
            });
    }
    
    /**
     * get current user
     */
    _getCurrentUser( username: any ): Observable<string> {
        let params = new URLSearchParams();
        params.append( 'username', username );
        let headers = new Headers( {
            "Authorization": "Bearer " + sessionStorage.getItem( 'access_token' )
        });
        let options = new RequestOptions( { headers: headers, search: params });
        return this._http.get( `${this.userEndPoint}`, options ).map(( response: Response ) => {
            sessionStorage.setItem( 'currentUser', JSON.stringify( { username: username, userId: response.json().body.id, status: response.json().body.status, access_token: this.access_token, refresh_token: this.refresh_token }) )
            return response.json().body.id;
        });
    }

    /**
     * Logout function
     */
    logout(): Observable<boolean> {
        let params = new URLSearchParams();
        let headers = new Headers( {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        });
        return this._http.post( `${this.logoutEndPoint}`, params.toString(), { headers: headers }).map(( response: Response ) => { return false; });
    }
    
    /**
     * Check token validity
     */
    tokenNotExpired(): Observable<boolean> {
        let headers = new Headers( {
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        });
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let params = new URLSearchParams();
            params.append( 'token', currentUser.access_token );
            return this._http.post( `${this.baseUrl}/oauth/check_token?${params.toString()}`, params.toString(),  )
                .map(
                ( results: Response ) => {
                    this.logged = true;
                    return Observable.of( this.logged );
                }
                ).catch( err => { return Observable.of( false ); });
        } else {
            this.logged = false;
            return Observable.of( this.logged );
        }
    }
    
    /**
     * 
     */
    private options(): any {
        // create authorization header with jwt token
        let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
        if ( currentUser && currentUser.access_token ) {
            let headers = new Headers();
            let currentUser = JSON.parse( sessionStorage.getItem( 'currentUser' ) );
            headers.append( 'Content-Type', 'application/json' );
            headers.append( 'Access-Control-Allow-Origin', '*' );
            headers.append( 'Authorization', 'Bearer ' + currentUser.access_token );
            return new RequestOptions( { headers: headers });
        }
    }
}