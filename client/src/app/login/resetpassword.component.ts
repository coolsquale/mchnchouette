import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AuthenticationService } from './authentification.service';
declare var System: any;

@Component(
    {
        selector: 'uzo-resetpassword',
        templateUrl: './resetpassword.component.html'
    }
)
export class ResetPassword implements OnInit {

     error = '';
     errorMessage:string; 
     errorCode:number;
    constructor( private router: Router, private _authenticationService: AuthenticationService ) {
    }

    resetpassword( event: any, username: any ) {
        event.preventDefault();
        this._authenticationService.resetpassword(username)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/home']);
                } else {
                    this.error = 'Username is not recognized';
                }
            },error => {
                //this.errorMessage=data.body.message;
                //System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                console.log(error);
                });
    }

    ngOnInit() {
        this._authenticationService.logout()
    }

}
