import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AuthenticationService } from './authentification.service';


@Component(
    {
        selector: 'uzo-activateaccount',
        templateUrl: './activateaccount.component.html'
    }
)
export class ActivateAccount implements OnInit {

     returnUrl: string;
     token: FormControl;
     activateAccountForm: FormGroup;
     valid: boolean = true;
     submitted: boolean = false;
     isValidated: boolean = false;

    constructor( private router: Router, private activatedRoute: ActivatedRoute, private _fb: FormBuilder, private _authenticationService: AuthenticationService ) {
    }

    ngOnInit() {
        this.token = new FormControl( '', [Validators.required] );
        this.activateAccountForm = this._fb.group( {
            'token': this.token
        });
        this.returnUrl = '/users';
    }

    /**
     * 
     */
    activateaccount( valid: boolean ) {

        this.isValidated = false;
        this.submitted = false;
        this.valid = valid;
        if ( !valid ) {
            return;
        }

        this._authenticationService.validateToken( this.activateAccountForm.controls["token"].value )
            .subscribe( result => {
                if ( result !== true ) {
                    this.isValidated = false;
                } else {
                    this.isValidated = true;
                    this.submitted = true;
                }
            },
            error => {
                this.submitted = true;
                console.log( 'An error occured' );
            },
            () => {
                if ( this.isValidated ) {
                    this.router.navigateByUrl(this.returnUrl);
                    //                    this.router.navigateByUrl('users/dashboard'); 
                }
            });
    }

}
