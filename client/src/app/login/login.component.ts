import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AuthenticationService } from './authentification.service';
import { USER_STATUS } from '../utils/constants'
declare var System: any;

@Component(
    {
        selector: 'uzo-login',
        templateUrl: './login.component.html'
    }
)
export class Login implements OnInit {
    private returnUrl: string;
    public isLoggedIn: boolean = false;
    public error = '';
    public id: string;
    public insertForm: FormGroup;
    public username: FormControl;
    public password: FormControl;
    public valid:boolean = true;
    public submitted:boolean = false;
    public errorMessage:string; 
    public errorCode:number;
    public redirect: boolean = false; 
    constructor( private _route: ActivatedRoute,private _fb: FormBuilder, private router: Router, private _authenticationService: AuthenticationService ) {
    
    }

    login( valid: boolean ) {

        this.valid = valid;
        if ( !valid ) {
            return;
        }

        this._authenticationService.login( this.insertForm.controls["username"].value, this.insertForm.controls["password"].value )
            .subscribe( result => { this.isLoggedIn = result; },
            error => {
                this.submitted = true;
                console.log( 'Username or password is incorrect' );
                console.log( error );
            },
            () => {
                if ( this.isLoggedIn == true ) {
                    this._authenticationService._getCurrentUser( this.insertForm.controls["username"].value ).subscribe( data => this.id = data,
                            error => {
                                //this.errorMessage=data.body.message;
                                //System.import('../../assets/js/theme-scripts.js').then(MyModule=>MyModule.showError());
                                console.log(error);
                                },
                        () => {
                            let currentUser = JSON.parse( localStorage.getItem( 'currentUser' ) );
                            if ( currentUser && currentUser.access_token ) {
                                if (currentUser && currentUser.status===USER_STATUS.UNDER_REVIEW) {
                                    this.router.navigate( ['login/activate'] );
                                }
                                let id: number = currentUser.userId;
                            }
                            this.insertForm.reset();
                            this.router.navigate( [this.returnUrl] );
                        })
                } else {
                    // redirect to error page, display message (in red) on screen
                }
            });
    }

    signup( event: any ) {
        event.preventDefault();
        this.router.navigate( ['/signup'] );
    }

    logout( event: any ) {
        event.preventDefault();
        this._authenticationService.logout()
        this.router.navigate( ['/home'] );
    }

    resetpassword( event: any ) {
        event.preventDefault();
        this.router.navigate( ['/resetpassword'] );
    }

    ngOnInit() {
        this._authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/users';
        this.redirect = this._route.snapshot.queryParams['returnUrl']? true : false;
        console.log("redirect = " + this.redirect);
        this.username = new FormControl( '', [Validators.required] );
        this.password = new FormControl( '', [Validators.required] );
        this.insertForm = this._fb.group( {
            'username': this.username,
            'password': this.password,
        });
    }

}
